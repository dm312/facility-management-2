/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.dal;

import facility.management.model.facility.FacilityAddressImpl;
import facility.management.model.facility.FacilityImpl;
import facility.management.model.useage.InspectionImpl;
import facility.management.model.useage.LeaseInfoImpl;
import facility.management.model.useage.LeasePaymentImpl;
import facility.management.model.useage.TenantImpl;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author dmartin3
 */
public class FacilityUsageDAO extends MyFunctionsImpl {

    public boolean isInUseDuringInterval(int facilityId, Date sDate, Date eDate) {
        /*
        Connection con = conn.globalCon();
        String facilityExists = exist("Facility", "facilityId", facilityId);
        if (facilityExists.equals("0")) {
            System.out.println("No FacilityImpl Record found");
        } else {
            try {
                LeaseInfoImpl leaseInfoImpl = this.getLeaseInfoImplFromFacilityId(facilityId);
                Date earliest = leaseInfoImpl.getStartDate();
                Date latest = leaseInfoImpl.getEndDate();

                if (earliest.after(eDate)) {
                    return true;
                }
                if (latest.before(sDate)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
            }
        }
        return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        boolean isInUse = false;
        try {
            tx = session.beginTransaction();
            LeaseInfoImpl leaseInfo = null;
            List leaseInfos = session.createQuery("FROM LeaseInfo WHERE facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = leaseInfos.iterator(); iterator.hasNext();) {
                leaseInfo = (LeaseInfoImpl) iterator.next();
            }
            Date startDate = leaseInfo.getStartDate();
            Date endDate = leaseInfo.getEndDate();
            if (endDate.before(sDate)) {
                isInUse = true;
                return isInUse;
            }
            if (startDate.after(eDate)) {
                isInUse = true;
                return isInUse;
            } else {
                isInUse = false;
                return isInUse;
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return isInUse;
    }

    public LeaseInfoImpl getLeaseInfoImplFromFacilityId(int facilityId) {
        /*
        Connection con = conn.globalCon();
        String leaseExists = exist("LeaseInfo", "facilityId", facilityId);
        if (leaseExists.equals("0")) {
            System.out.println("LeaseInfoImpl Record not found");
        } else {
            try {
                String sql = "SELECT leaseInfoId, tenantId,startDate,endDate,securityDeposit,terminationDate,status from LeaseInfo where facilityId='" + facilityId + "'";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                LeaseInfoImpl leaseInfo = new LeaseInfoImpl();
                while (rs.next()) {
                    leaseInfo.setLeaseInfoId(rs.getInt("leaseInfoId"));

                    int tenantImplId = rs.getInt("tenantId");
                    TenantImpl tenantImpl = this.getTenantImplFromTenantId(tenantImplId);
                    leaseInfo.setTenantImpl(tenantImpl);
                    leaseInfo.setStartDate(rs.getDate("startDate"));
                    leaseInfo.setEndDate(rs.getDate("endDate"));
                    leaseInfo.setSecurityDeposit(rs.getBigDecimal("securityDeposit"));
                    leaseInfo.setTerminationDate(rs.getDate("terminationDate"));
                    leaseInfo.setStatus(rs.getString("status"));

                }
                rs.close();
                st.close();
                con.close();
                return leaseInfo;

            } catch (Exception e) {
            }
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        LeaseInfoImpl leaseInfo = null;
        try {
            tx = session.beginTransaction();
            List leaseInfos = session.createQuery("FROM LeaseInfo WHERE facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = leaseInfos.iterator(); iterator.hasNext();) {
                leaseInfo = (LeaseInfoImpl) iterator.next();
            }
            tx.commit();
            return leaseInfo;
        } catch (HibernateException e) {
            if (/*e*/tx != null) {
                /*e*/
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public FacilityAddressImpl getFacilityAddressImplFromId(int facilityAddressId) {
        /*
        Connection con = conn.globalCon();
        String facilityAddressExists = exist("FacilityAddress", "facilityAddressId", facilityAddressId);
        if (facilityAddressExists.equals("0")) {
            System.out.println("FacilityAddressImpl Record does not exist from getFacilityAddressImplFromId()");
        } else {
            try {
                FacilityAddressImpl facilityAddressImpl = new FacilityAddressImpl();
                String facilityAddressSelectQuery = "select streetAddress, city,state,zipcode from FacilityAddress where facilityAddressId ='" + facilityAddressId + "'";
                Statement facilityAddressStatement = con.createStatement();
                ResultSet facilityAddressResultSet = facilityAddressStatement.executeQuery(facilityAddressSelectQuery);
                System.out.println("FacilityMaintenanceDAO:***************" + facilityAddressSelectQuery);
                while (facilityAddressResultSet.next()) {
                    facilityAddressImpl.setFacilityAddressId(facilityAddressId);
                    facilityAddressImpl.setStreetAddress(facilityAddressResultSet.getString("streetAddress"));
                    facilityAddressImpl.setCity(facilityAddressResultSet.getString("city"));
                    facilityAddressImpl.setState(facilityAddressResultSet.getString("state"));
                    facilityAddressImpl.setZipCode(facilityAddressResultSet.getString("zipcode"));

                    System.out.println("1 FacilityAddress Record Selected from getFacilityAddressImplFromId()");

                }
                facilityAddressStatement.close();
                facilityAddressResultSet.close();
                System.out.println("End of Query**************** :FacilityMaintenanceDAO");
                return facilityAddressImpl;
            } catch (Exception e) {
            };
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        FacilityAddressImpl facilityAddress = null;
        try {
            tx = session.beginTransaction();
            List facilityAddresses = session.createQuery("FROM FacilityAddress WHERE facility_Address_Id ='" + facilityAddressId + "'").list();
            for (Iterator iterator
                    = facilityAddresses.iterator(); iterator.hasNext();) {
                facilityAddress = (FacilityAddressImpl) iterator.next();
            }
            tx.commit();
            return facilityAddress;
        } catch (HibernateException e) {
            if (/*e*/tx != null) {
                /*e*/
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public TenantImpl getTenantImplFromTenantId(int tenantId) {
        /*
        TenantImpl tenantImpl = new TenantImpl();
        Connection con = conn.globalCon();
        String tenantExists = exist("Tenant", "tenantId", tenantId);
        if (tenantExists.equals("0")) {
            System.out.println("No TenantImpl Record found");
        } else {
            try {
                String sql = "select tenantId, firstName, lastName, ssn, email, phoneId,address from Tenant where tenantId ='" + tenantId + "'";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    tenantImpl.setTenantId(tenantId);
                    tenantImpl.setFirstName(rs.getString("firstName"));
                    tenantImpl.setLastName(rs.getString("lastName"));
                    tenantImpl.setssn(rs.getString("ssn"));
                    tenantImpl.setPhone(rs.getString("phone"));
                    tenantImpl.setAddress(rs.getString("address"));
                    tenantImpl.setEmail(rs.getString("email"));
                }
                rs.close();
                st.close();
                con.close();
                return tenantImpl;

            } catch (Exception e) {
            }
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        TenantImpl tenant = null;
        try {
            tx = session.beginTransaction();
            List tenants = session.createQuery("FROM Tenant WHERE tenant_Id ='" + tenantId + "'").list();
            for (Iterator iterator
                    = tenants.iterator(); iterator.hasNext();) {
                tenant = (TenantImpl) iterator.next();
            }
            tx.commit();
            return tenant;
        } catch (HibernateException e) {
            if (/*e*/tx != null) {
                /*e*/
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public FacilityImpl getFacilityImplFromId(int facilityId) {
        /*
        Connection con = conn.globalCon();
        String facilityImplExists = exist("Facility", "facilityId", facilityId);
        if (facilityImplExists.equals("0")) {
            System.out.println("FacilityImpl Record " + "Number: " + facilityId + " Does Not Exist from getFacilityImplFromId()");
            return null;
        } else {
            try {
                FacilityImpl facilityImpl = new FacilityImpl();
                String facilitySelectQuery = "Select facilityAddressId,facilityName,capacity,totalArea,facilityType from Facility where facilityId='" + facilityId + "'";
                Statement facilityImplStatement = con.createStatement();
                ResultSet facilitySelectResultSet = facilityImplStatement.executeQuery(facilitySelectQuery);

                while (facilitySelectResultSet.next()) {
                    facilityImpl.setFacilityId(facilityId);
                    FacilityAddressImpl facilityAddressImpl = new FacilityAddressImpl();

                    int facilityAddressId = facilitySelectResultSet.getInt("facilityAddressId");
                    facilityAddressImpl = this.getFacilityAddressImplFromId(facilityAddressId);
                    facilityImpl.setFacilityAddressImpl(facilityAddressImpl);
                    facilityImpl.setFacilityName(facilitySelectResultSet.getString("facilityName"));
                    facilityImpl.setCapacity(facilitySelectResultSet.getInt("capacity"));
                    facilityImpl.setTotalArea(facilitySelectResultSet.getBigDecimal("totalArea"));
                    facilityImpl.setFacilityType(facilitySelectResultSet.getString("facilityType"));

                }
                facilitySelectResultSet.close();
                facilityImplStatement.close();
                return facilityImpl;
            } catch (Exception e) {
            }
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        FacilityImpl facility = null;
        try {
            tx = session.beginTransaction();
            List facilities = session.createQuery("FROM Facility where facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = facilities.iterator(); iterator.hasNext();) {
                facility = (FacilityImpl) iterator.next();
            }
            tx.commit();
            return facility;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public boolean assignFacilityToUse(FacilityImpl facilityImpl, TenantImpl tenantImpl, Date startDate, Date endDate, BigDecimal securityDeposit, Date terminationDate, String status) {
        Integer leaseInfoImplId = null;
        Session assignSession = null;
        Transaction tx = null;
        try {
            tx = assignSession.beginTransaction();
            LeaseInfoImpl leaseInfo = new LeaseInfoImpl(facilityImpl, tenantImpl, startDate, endDate, securityDeposit, terminationDate, status);
            leaseInfoImplId = (Integer) assignSession.save(leaseInfo);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            assignSession.close();
        }
        return false;
        /*
        Connection con = conn.globalCon();
        String leaseExists = exist("LeaseInfo", "leaseInfoId", leaseInfoImpl.getLeaseInfoId());
        if (leaseExists.equals("0")) {
            System.out.println("No LeaseInfoImpl Record found");
        } else {
            try {
                String sql = "insert into LeaseInfo (leaseInfoId,facilityId,tenantId,"
                        + "startDate,endDate"
                        + ",securityDeposit,terminationDate,status values (?,?,?,?,?,?,?,?)";

                System.out.println("FacilityUsageDAO:*************" + sql);

                PreparedStatement ps = con.prepareStatement(sql);
                ps.setInt(1, leaseInfoImpl.getLeaseInfoId());
                ps.setInt(2, leaseInfoImpl.getFacilityImpl().getFacilityId());
                ps.setInt(3, leaseInfoImpl.getTenantImpl().getTenantId());
                ps.setDate(4, new java.sql.Date(leaseInfoImpl.getStartDate().getTime()));
                ps.setDate(5, new java.sql.Date(leaseInfoImpl.getEndDate().getTime()));
                ps.setBigDecimal(6, leaseInfoImpl.getSecurityDeposit());
                ps.setDate(7, new java.sql.Date(leaseInfoImpl.getTerminationDate().getTime()));
                ps.setString(8, leaseInfoImpl.getStatus());

                int numberOfRowsInserted = ps.executeUpdate();
                ps.close();
                con.close();
                System.out.println(numberOfRowsInserted + " LeaseInfoImpl Records added from assignFacilityToUse()");
                System.out.println("End of Query*****************: FacilityUsageDAO");
                return true;
            } catch (Exception e) {
            }
        }
        return false;
         */

    }

    public boolean vacateFacility(int facilityId, Date latest) {
        /*
        Connection con = conn.globalCon();
        String leaseInfoExists = this.exist("LeaseInfo", "facilityId", facilityId);
        if (leaseInfoExists.equals("0")) {
            System.out.println("LeaseInfo Record not found");
        } else {
            try {
                String sql = "update LeaseInfo set terminationDate = ?, status = ? where facilityId ='" + facilityId;
                PreparedStatement ps = con.prepareStatement(sql);

                ps.setDate(1, new java.sql.Date(latest.getTime()));
                ps.setString(2, LeaseStatus.TERMINATED.toString());

                int numberOfRowsUpdated = ps.executeUpdate();
                System.out.println("FacilityUsageDAO:***************" + sql);
                System.out.println(numberOfRowsUpdated + "LeaseInfoImpl Record updated");
                System.out.println("End of Query *************** :FacilityUsageDAO");
                return true;
            } catch (Exception e) {
            }
        }
        return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        boolean result = false;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(LeaseInfoImpl.class);
            cr.add(Restrictions.eq("facility_Id", facilityId));
            List leaseInfoResults = cr.list();
            LeaseInfoImpl leaseInfo = (LeaseInfoImpl) leaseInfoResults.get(0);
            leaseInfo.setTerminationDate(latest);
            session.update(leaseInfo);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public List<InspectionImpl> listInspections() {
        /*
        Connection con = conn.globalCon();
        List<InspectionImpl> inspectionImplList = new ArrayList<InspectionImpl>();
        try {
            String sql = "select inspectionId, facilityId,inspectionDate,inspectionResult from Inspection";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next() == false) {
                System.out.println("No Inspection Record found");
            } else {
                rs.beforeFirst();
                int totalAdds = 0;
                while (rs.next()) {
                    System.out.println("FacilityUsageDAO:****************" + sql);
                    InspectionImpl inspectionImpl = new InspectionImpl();
                    FacilityImpl facilityImpl = this.getFacilityImplFromId(rs.getInt("facilityId"));
                    inspectionImpl.setFacilityImpl(facilityImpl);
                    inspectionImpl.setInspectionDate(rs.getDate("inspectionDate"));
                    inspectionImpl.setInspectionResult(rs.getString("inspectionResult"));

                    inspectionImplList.add(inspectionImpl);
                    System.out.println("End of Query ************* :FacilityUsageDAO");
                    totalAdds++;
                }
                System.out.println(totalAdds + "InspectionImpl Records Added to List");
                rs.close();
                st.close();
                con.close();
            }
            return inspectionImplList;
        } catch (Exception e) {
        }

        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        List<InspectionImpl> allInspectionsList = new ArrayList<InspectionImpl>();
        try {
            tx = session.beginTransaction();
            List inspections = session.createQuery("FROM Inspection").list();
            for (Iterator iterator
                    = inspections.iterator(); iterator.hasNext();) {
                InspectionImpl inspection = (InspectionImpl) iterator.next();
                allInspectionsList.add(inspection);
            }
            tx.commit();
            return allInspectionsList;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return allInspectionsList;
    }

    public HashMap<Integer, Integer> listActualUsage() {
        /*
        Connection con = conn.globalCon();
        HashMap<Integer, Integer> listInfo = new HashMap<Integer, Integer>();

        try {
            String sql = "select startDate, endDate, facilityId from LeaseInfo";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int numberOfRowsSelected = 0;
            while (rs.next()) {
                System.out.println("FacilityUsageDAO:***************" + sql);
                long Existence = rs.getDate("endDate").getTime() - rs.getDate("startDate").getTime();
                int facilityId = rs.getInt("facilityId");
                listInfo.put(facilityId, (int) Existence);
                numberOfRowsSelected++;
                System.out.println("End of Query ***************88 :FacilityUsageDAO");
            }
            System.out.println(numberOfRowsSelected + " LeaseInfoImpl Record Info Tables added");
            return listInfo;
        } catch (Exception e) {
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        HashMap<Integer, Integer> actualUsageList = new HashMap<Integer, Integer>();
        try {
            tx = session.beginTransaction();
            List leaseInfos = session.createQuery("FROM LeaseInfo").list();
            for (Iterator iterator
                    = leaseInfos.iterator(); iterator.hasNext();) {
                LeaseInfoImpl leaseInfo = (LeaseInfoImpl) iterator.next();
                Integer facilityId = leaseInfo.getFacilityImpl().getFacilityId();
                Long existence = leaseInfo.getEndDate().getTime() - leaseInfo.getStartDate().getTime();
                int existenceCasted = existence.intValue();
                actualUsageList.put(facilityId, existenceCasted);
            }
            tx.commit();
            return actualUsageList;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return actualUsageList;
    }

    public HashMap<Integer, BigDecimal> calcUsageRate() {
        /*
        HashMap<Integer,BigDecimal> listInfo = new HashMap<Integer,BigDecimal>();
        Connection con = conn.globalCon();
      try
            {
                BigDecimal bd = BigDecimal.ZERO;
                String sql ="select amountDue, amountPaid, leasePaymentId from LeasePayment";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                int rowsSelected = 0;
                while(rs.next())
                {
                    rowsSelected++;
                    System.out.println("FacilityUsageDAO:******************" + sql);
                    int cost2 = rs.getInt("amountPaid") - rs.getInt("amountDue");
                    bd = new java.math.BigDecimal(String.valueOf(cost2));
                    int id = rs.getInt("leasePaymentId");
                    listInfo.put(id, bd);
                    System.out.println(rowsSelected+ " LeasePaymentImpl Records added to list");
                    
                }
                rs.close();
                st.close();
                con.close();
                System.out.println("End of query ***************** :FacilityUsageDAO");
                return listInfo;
            }
            catch(Exception e)
            {}
        
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        HashMap<Integer, BigDecimal> listInfo = new HashMap<Integer, BigDecimal>();
        try {
            tx = session.beginTransaction();
            List leasePayments = session.createQuery("FROM LeasePayment").list();
            for (Iterator iterator
                    = leasePayments.iterator(); iterator.hasNext();) {
                LeasePaymentImpl leasePayment = (LeasePaymentImpl) iterator.next();
                BigDecimal subtrahend = leasePayment.getAmountPaid();
                BigDecimal useageRate = leasePayment.getAmountDue().subtract(subtrahend);
                Integer leasePaymentId = leasePayment.getLeasePaymentId();
                listInfo.put(leasePaymentId, useageRate);
            }
            tx.commit();
            return listInfo;
        } catch (HibernateException e) {
            if (e != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return listInfo;
    }
    ApplicationContext context = new ClassPathXmlApplicationContext("facility.management.dal.daos.xml");
    DBHelper conn = (DBHelper) context.getBean("dBHelper");//new DBHelper();    
    private static SessionFactory factory;
    private static SessionFactory factory2;
    private static SessionFactory factory3;
    private static SessionFactory factory4;
}
