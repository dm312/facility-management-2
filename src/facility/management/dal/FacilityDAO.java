/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.dal;

import facility.management.model.facility.FacilityAddressImpl;
import facility.management.model.facility.FacilityImpl;
import facility.management.model.facility.BuildingImpl;
import facility.management.model.facility.BuildingUnitImpl;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 *
 * @author dmartin3
 */
public class FacilityDAO extends MyFunctionsImpl {

    public boolean addNewFacility(FacilityAddressImpl facilityAddressImpl, String facilityName, Integer capacity, BigDecimal totalArea, String facilityType) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer facilityId = null;
        try {
            tx = session.beginTransaction();
            FacilityImpl facility = new FacilityImpl(facilityAddressImpl, facilityName, capacity, totalArea, facilityType);
            facilityId = (Integer) session.save(facility);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        /* Connection con = conn.globalCon();

        PreparedStatement facilityAddressStatement = null;
        PreparedStatement facilityStatement = null;
        MyFunctionsImpl test = new MyFunctionsImpl();
        String facilityAddressExists = test.exist("FacilityAddress", "facilityAddressId", facilityImpl.getFacilityAddressImpl().getFacilityAddressId());
        if (facilityAddressExists.equals("0")) {
            try {
                FacilityAddressImpl facilityAddress = facilityImpl.getFacilityAddressImpl();
                String facilityAddressInsertQuery = "insert into FacilityAddress (facilityAddressId,streetAddress,city,state,zipcode) values (?,?,?,?,?)";
                facilityAddressStatement = con.prepareStatement(facilityAddressInsertQuery);

                facilityAddressStatement.setInt(1, facilityAddress.getFacilityAddressId());
                facilityAddressStatement.setString(2, facilityAddress.getStreetAddress());
                facilityAddressStatement.setString(3, facilityAddress.getCity());
                facilityAddressStatement.setString(4, facilityAddress.getState());
                facilityAddressStatement.setString(5, facilityAddress.getZipCode());
                int numberOfRowsInserted = facilityAddressStatement.executeUpdate();

                facilityAddressStatement.close();
                System.out.println(numberOfRowsInserted + " FacilityAddressImpl Record Added from addNewFacility()");
            } catch (Exception e) {
            }*/
        return false;
        /*}

        String facilityExists = test.exist("Facility", "facilityId", facilityImpl.getFacilityId());
        if (facilityExists.equals("0")) {
            try {
                String facilityInsertQuery = "insert into Facility (facilityId, facilityAddressId,facilityName,capacity,totalArea,facilityType) values (?,?,?,?,?,?)";
                facilityStatement = con.prepareStatement(facilityInsertQuery);
                facilityStatement.setInt(1, facilityImpl.getFacilityId());
                facilityStatement.setInt(2, facilityImpl.getFacilityAddressImpl().getFacilityAddressId());
                facilityStatement.setString(3, facilityImpl.getFacilityName());
                facilityStatement.setInt(4, facilityImpl.getCapacity());
                facilityStatement.setBigDecimal(5, facilityImpl.getTotalArea());
                facilityStatement.setString(6, facilityImpl.getFacilityType());

                int numberOfRowsInserted = facilityStatement.executeUpdate();
                System.out.println(numberOfRowsInserted + " FacilityImpl Record Added from addNewFacility()");
                return true;
            } catch (Exception e) {
            }
        }
        return false;*/
    }

    public BuildingImpl addBuildingDetails(FacilityImpl facility, int numOfElevators, int numOfFloors, int numOfEntrances) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer buildingId = null;
        try {
            tx = session.beginTransaction();
            BuildingImpl building = new BuildingImpl(facility, numOfElevators, numOfFloors, numOfEntrances);
            buildingId = (Integer) session.save(building);
            tx.commit();
            return building;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
        /* Connection con = conn.globalCon();
        PreparedStatement buildingStatement = null;
        PreparedStatement facilityStatement = null;
        PreparedStatement facilityAddressStatement = null;
        MyFunctionsImpl test = new MyFunctionsImpl();
        FacilityImpl facilityImpl = buildingImpl.getFacilityImpl();
        String facilityAddressExists = test.exist("FacilityAddress", "facilityAddressId", facilityImpl.getFacilityAddressImpl().getFacilityAddressId());
        if (facilityAddressExists.equals("0")) {
            try {
                FacilityAddressImpl facilityAddress = facilityImpl.getFacilityAddressImpl();
                String facilityAddressInsertQuery = "insert into FacilityAddress (facilityAddressId,streetAddress,city,state,zipcode) values (?,?,?,?,?)";
                facilityAddressStatement = con.prepareStatement(facilityAddressInsertQuery);

                facilityAddressStatement.setInt(1, facilityAddress.getFacilityAddressId());
                facilityAddressStatement.setString(2, facilityAddress.getStreetAddress());
                facilityAddressStatement.setString(3, facilityAddress.getCity());
                facilityAddressStatement.setString(4, facilityAddress.getState());
                facilityAddressStatement.setString(5, facilityAddress.getZipCode());
                int numberOfRowsInserted = facilityAddressStatement.executeUpdate();

                facilityAddressStatement.close();
                System.out.println(numberOfRowsInserted + " FacilityAddressImpl Record Added from addBuildingDetails()");
            } catch (Exception e) {
            }
            
        }
        String facilityExists = test.exist("Facility", "facilityId", buildingImpl.getFacilityImpl().getFacilityId());
        if (facilityExists.equals("0")) {
            try {
                String facilityInsertQuery = "insert into Facility (facilityId, facilityAddressId,facilityName,capacity,totalArea,facilityType) values (?,?,?,?,?,?)";
                facilityStatement = con.prepareStatement(facilityInsertQuery);
                facilityStatement.setInt(1, facilityImpl.getFacilityId());
                facilityStatement.setInt(2, facilityImpl.getFacilityAddressImpl().getFacilityAddressId());
                facilityStatement.setString(3, facilityImpl.getFacilityName());
                facilityStatement.setInt(4, facilityImpl.getCapacity());
                facilityStatement.setBigDecimal(5, facilityImpl.getTotalArea());
                facilityStatement.setString(6, facilityImpl.getFacilityType());

                int numberOfRowsInserted = facilityStatement.executeUpdate();
                System.out.println(numberOfRowsInserted + " FacilityImpl Record Added from addBuildingDetails()");
            } catch (Exception e) {
            }
        }
        String buildingExists = test.exist("Building", "buildingId", buildingImpl.getBuildingId());
        if (buildingExists.equals("0")) {
            try {
                String buildingInsertQuery = "insert into Building(buildingId,facilityId,numOfElevators,numOfFloors,numOfEntrances) values(?,?,?,?,?)";
                buildingStatement = con.prepareStatement(buildingInsertQuery);
                buildingStatement.setInt(1, buildingImpl.getBuildingId());
                buildingStatement.setInt(2, buildingImpl.getFacilityImpl().getFacilityId());
                buildingStatement.setInt(3, buildingImpl.getNumOfElevators());
                buildingStatement.setInt(4, buildingImpl.getNumOfFloors());
                buildingStatement.setInt(5, buildingImpl.getNumOfEntrances());

                int numberOfRowsInserted = buildingStatement.executeUpdate();
                System.out.println(numberOfRowsInserted + " BuildingImpl Record Added from addBuildingDetails()");
                return true;
            } catch (Exception e) {
            }
        }
        return false;*/
    }

    public BuildingUnitImpl addBuildingUnitDetails(FacilityImpl facility, Integer levelNum, Integer numOfRooms) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer buildingUnitId = null;
        BuildingUnitImpl buildingUnit = null;
        try {
            tx = session.beginTransaction();
            buildingUnit = new BuildingUnitImpl(facility, levelNum, numOfRooms);
            buildingUnitId = (Integer) session.save(buildingUnit);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return buildingUnit;

        /* Connection con = conn.globalCon();

        PreparedStatement buildingUnitStatement = null;
        MyFunctionsImpl test = new MyFunctionsImpl();

        String buildingUnitExists = test.exist("BuildingUnit", "buildingUnitId", buildingUnitImpl.getBuildingUnitId());
        if (buildingUnitExists.equals("0")) {
            try {
                String buildingUnitInsertQuery = "insert into BuildingUnit (buildingUnitId,facilityId,levelNum,numOfRooms) values (?,?,?,?)";
                buildingUnitStatement = con.prepareStatement(buildingUnitInsertQuery);

                buildingUnitStatement.setInt(1, buildingUnitImpl.getBuildingUnitId());
                buildingUnitStatement.setInt(2, buildingUnitImpl.getFacilityImpl().getFacilityId());
                buildingUnitStatement.setInt(3, buildingUnitImpl.getLevelNum());
                buildingUnitStatement.setInt(4, buildingUnitImpl.getNumOfRooms());

                int numberOfRowsInserted = buildingUnitStatement.executeUpdate();

                buildingUnitStatement.close();
                System.out.println(numberOfRowsInserted + " BuildingUnitImpl Record Added from addBuildingUnitDetails()");
            } catch (Exception e) {
                e.printStackTrace();
            }
            addNewFacility(buildingUnitImpl.getFacilityImpl());
        }
        return false;*/
    }

    public void getFacilityImplInfo(int facilityImplId) {
        /*  Connection con = conn.globalCon();
        try {
            String selectFacilityTypeQuery = "select facilityType from Facility where facilityId = '" + facilityImplId + "'";
            Statement st = con.createStatement();
            st.executeQuery(selectFacilityTypeQuery);
            ResultSet rs = st.getResultSet();
            System.out.println("FacilityDAO: *************** Query " + selectFacilityTypeQuery);

            String facilityType = "";
            if (rs.next() == false) {
                System.out.println("FacilityType is required from getFacilityImplInfo");
            } else {
                rs.beforeFirst();
                while (rs.next()) {
                    facilityType = rs.getString("facilityType");
                    System.out.println("Facility Type is: " + facilityType);
                }
                st.close();
                rs.close();

                if (facilityType.equals(FacilityType.BUILDING)) {
                    System.out.println();
                    return getBuildingImplInfo(facilityImplId);
                }
                if (facilityType.equals(FacilityType.UNIT)) {
                    System.out.println();
                    return getBuildingImplInfo(facilityImplId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;*/
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List facilities = session.createQuery("FROM Facility where facility_Id ='" + facilityImplId + "'").list();
            for (Iterator iterator
                    = facilities.iterator(); iterator.hasNext();) {
                FacilityImpl facility = (FacilityImpl) iterator.next();
                System.out.println("Facility Id:" + facilityImplId + " has type : " + facility.getFacilityType());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void getBuildingImplInfo(int facilityImplId) {
        /* Connection con = conn.globalCon();
        try {
            Statement buildingImplStatement = con.createStatement();
            String sql = "select f.facilityName, f.capacity, f.facilityType, b.buildingId, b.numOfFloors, b.numOfElevators, b.numOfEntrances from Facility as f, Building as b where f.facilityId= b.facilityId AND f.facilityId = '" + facilityImplId + "'";
            buildingImplStatement.executeQuery(sql);
            ResultSet rs = buildingImplStatement.getResultSet();
            System.out.println("FacilityDAO: *************** Query " + sql);

            BuildingImpl building = new BuildingImpl();
            FacilityImpl facilityImpl = new FacilityImpl();

            if (rs.next() == false) {
                System.out.println("No Building Record exists from getBuildingImplInfo()");
            } else {
                rs.beforeFirst();
                while (rs.next()) {
                    building.setBuildingId(rs.getInt("buildingId"));
                    building.setNumOfElevators(rs.getInt("numOfElevators"));
                    building.setNumOfFloors(rs.getInt("numOfFloors"));
                    building.setNumOfEntrances(rs.getInt("numOfEntrances"));

                    facilityImpl.setFacilityId(facilityImplId);
                    facilityImpl.setFacilityName(rs.getString("facilityName"));
                    facilityImpl.setCapacity(rs.getInt("capacity"));
                    facilityImpl.setFacilityType(rs.getString("facilityType"));
                    building.setFacilityImpl(facilityImpl);

                    building.toString2();
                    facilityImpl.toString2();
                    System.out.println("End of Query*************** :FacilityDAO ");
                }
                buildingImplStatement.close();
                rs.close();
                return facilityImpl;
            }
        } catch (Exception e) {
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List buildingImpls = session.createQuery("FROM Building where facility_Id='" + facilityImplId + "'").list();
            for (Iterator iterator
                    = buildingImpls.iterator(); iterator.hasNext();) {
                BuildingImpl building = (BuildingImpl) iterator.next();
                FacilityImpl buildingFacility = building.getFacilityImpl();
                FacilityAddressImpl buildingFacilityFacilityAddressImpl = buildingFacility.getFacilityAddressImpl();
                System.out.println("Building number of Elevators : " + building.getNumOfElevators());
                System.out.println("Number of Floors : " + building.getNumOfFloors());
                System.out.println("Number of Entrances : " + building.getNumOfEntrances());
                System.out.println("Facility name : " + buildingFacility.getFacilityName());
                System.out.println("Facility Capacity: " + buildingFacility.getCapacity());
                System.out.println("Facility Total Area: " + buildingFacility.getTotalArea() + "mi/sq ft.");
                System.out.println("Facility Type: " + buildingFacility.getFacilityType());
                System.out.println("Facility Postal Address: " + buildingFacilityFacilityAddressImpl.getStreetAddress()
                        + buildingFacilityFacilityAddressImpl.getCity()
                        + "," + buildingFacilityFacilityAddressImpl.getState()
                        + buildingFacilityFacilityAddressImpl.getZipCode());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<FacilityImpl> listFacilities() {
        /*Connection con = conn.globalCon();
        Statement st = null;
        try {
            st = con.createStatement();
            String selectFacilityQuery = "Select facilityName, facilityId, facilityType, capacity from Facility";
            ResultSet rs = st.executeQuery(selectFacilityQuery);
            System.out.println("FacilityDAO:*************** Query" + selectFacilityQuery);
            List<FacilityImpl> facilityImplList = new ArrayList<FacilityImpl>();
            if (rs.next() == false) {
                System.out.println("No FacilityImpl record exists");
            } else {
                rs.beforeFirst();
                while (rs.next()) {
                    FacilityImpl facilityImpl = new FacilityImpl();
                    facilityImpl.setFacilityName(rs.getString("facilityName"));
                    facilityImpl.setFacilityId(rs.getInt("facilityId"));
                    facilityImpl.setFacilityType(rs.getString("facilityType"));
                    facilityImpl.setCapacity(rs.getInt("capacity"));
                    facilityImplList.add(facilityImpl);
                }
                st.close();
                rs.close();
                return facilityImplList;
            }
            System.out.println("End of Query***************: FacilityDAO");

        } catch (Exception e) {
        }
        return null;*/
        Session session = factory.openSession();
        Transaction tx = null;
        List<FacilityImpl> facilityList = new ArrayList<FacilityImpl>();
        try {
            tx = session.beginTransaction();
            List facilities = session.createQuery("FROM Facility").list();
            for (Iterator iterator
                    = facilities.iterator(); iterator.hasNext();) {
                FacilityImpl facility = (FacilityImpl) iterator.next();
                facilityList.add(facility);
            }
            tx.commit();
            return facilityList;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public long getAvailableCapacity(int facilityId) {

        /*  Connection con = conn.globalCon();
        try {
            Statement st = con.createStatement();
            String sql = "select capacity from Facility where facilityId = '" + facilityId + "'";
            ResultSet facilityRS = st.executeQuery(sql);
            System.out.println("FacilityDAO: *************** Query " + sql);
            Long capacity = null;
            while (facilityRS.next()) {
                capacity = facilityRS.getLong("capacity");
            }
            facilityRS.close();
            st.close();
            System.out.println("Facility Id " + facilityId + "capacity: " + capacity);
            System.out.println("End of Query***************: FacilityDAO");
            return capacity;
        } catch (Exception e) {
        }
        return null;*/
        long capacity = 0;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List facilities = session.createQuery("FROM Facility where facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = facilities.iterator(); iterator.hasNext();) {
                FacilityImpl facility = (FacilityImpl) iterator.next();
                capacity = facility.getCapacity();
            }
            tx.commit();
            return capacity;

        } //  capacity = session.createQuery("SELECT capacity FROM Facility WHERE facility_Id ='" + facilityId + "'");
        catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return capacity;
    }

    public boolean removeFacility(int facilityId) {
        /*
        Connection con = conn.globalCon();
        MyFunctionsImpl test = new MyFunctionsImpl();
        String facilityExists = test.exist("Facility", "facilityId", facilityId);
        if (facilityExists.equals("1")) {
            try {
                Statement facilityStatement = con.createStatement();
                String facilityDeleteQuery = "delete from Facility where facilityId='" + facilityId + "'";

                System.out.println("FacilityDAO:****************" + facilityDeleteQuery);
                int numberOfRowsDeleted = facilityStatement.executeUpdate(facilityDeleteQuery);
                System.out.println(numberOfRowsDeleted + " FacilityImpl Record deleted");
                System.out.println("End of Query*************** :FacilityDAO");
            } catch (Exception e) {
            }
        }
        return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            FacilityImpl facility = (FacilityImpl) session.get(FacilityImpl.class, facilityId);
            session.delete(facility);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public static void main(String[] args) {

    }
    ApplicationContext context = new ClassPathXmlApplicationContext(".daos.xml");
    DBHelper conn = (DBHelper) context.getBean("dBHelper");//new DBHelper();
    private static SessionFactory factory;
}
