/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.dal;

import facility.management.model.facility.FacilityAddressImpl;
import facility.management.model.facility.FacilityImpl;
import facility.management.model.maintenance.MaintenanceCostImpl;
import facility.management.model.maintenance.MaintenanceImpl;
import facility.management.model.maintenance.MaintenancePhoneCarrier;
import facility.management.model.maintenance.MaintenancePhoneType;
import facility.management.model.maintenance.MaintenanceRequestImpl;
import facility.management.model.maintenance.MaintenanceWorkerPhoneImpl;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author dmartin3
 */
public class FacilityMaintenanceDAO extends MyFunctionsImpl {

    public MaintenanceWorkerPhoneImpl getMaintenanceWorkerPhoneImplFromId(int workerPhoneId) {
        /*
        Connection con = conn.globalCon();
        String maintenanceWorkerPhoneImplExists = exist("MaintenancePhone","maintenancePhoneId",workerPhoneId);
        if(maintenanceWorkerPhoneImplExists.equals("0"))
        {
            System.out.println("Maintenance Worker Phone Record not found");
        }
        else
        {
            MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl = null;
            try
            {
                String maintenanceWorkerPhoneImplSelectQuery = "Select maintenancePhoneId, maintenancePhoneCarrier,maintenancePhoneType,maintenancePhoneNumber from MaintenancePhone";
                System.out.println("FacilityMaintenanceDAO:****************" + maintenanceWorkerPhoneImplSelectQuery);
                Statement maintenanceWorkerPhoneImplStatement = con.createStatement();
                ResultSet maintenanceWorkerPhoneImplResultSet = maintenanceWorkerPhoneImplStatement.executeQuery(maintenanceWorkerPhoneImplSelectQuery);
                
                while(maintenanceWorkerPhoneImplResultSet.next())
                {
                    maintenanceWorkerPhoneImpl.setPhoneId(workerPhoneId);
                    maintenanceWorkerPhoneImpl.setPhoneCarrier(MaintenancePhoneCarrier.valueOf(maintenanceWorkerPhoneImplResultSet.getString("maintenancePhoneCarrier")));
                    maintenanceWorkerPhoneImpl.setPhoneType(MaintenancePhoneType.valueOf(maintenanceWorkerPhoneImplResultSet.getString("maintenancePhoneType")));
                    maintenanceWorkerPhoneImpl.setPhoneNumber(maintenanceWorkerPhoneImplResultSet.getString("maintenancePhoneNumber"));
                }
                maintenanceWorkerPhoneImplStatement.close();
                maintenanceWorkerPhoneImplResultSet.close();
            System.out.println("1 MaintenanceWorkerPhoneImpl Record Selected from getMaintenanceWorkerPhoneImplFromId");
            System.out.println("End of Query **************: FacilityMaintenanceDAO");
            return maintenanceWorkerPhoneImpl;
            }
            catch(Exception e)
            {}
            
            
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenanceWorkerPhoneImplList = session.createQuery("FROM MaintenancePhone WHERE maintenance_Phone_Id ='" + workerPhoneId + "'").list();
            List<MaintenanceWorkerPhoneImpl> maintenanceWorkerPhones = new ArrayList<MaintenanceWorkerPhoneImpl>();
            for (Iterator iterator
                    = maintenanceWorkerPhoneImplList.iterator(); iterator.hasNext();) {
                MaintenanceWorkerPhoneImpl maintenanceWorkerPhone = (MaintenanceWorkerPhoneImpl) iterator.next();
                maintenanceWorkerPhones.add(maintenanceWorkerPhone);
            }
            tx.commit();
            return maintenanceWorkerPhones.get(0);
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public boolean addFacilityMaintenanceImplRequest(FacilityImpl facilityImpl, Date requestDate, String nameOfRequester, String detail, String email, String subject) {
        /*
       Connection con = conn.globalCon();
       PreparedStatement maintenanceRequestImplStatement = null;
       try
       {
           String maintenanceRequestInsertQuery = "insert into MaintenanceRequest (maintenanceRequestId,requestDate,facilityId,nameOfRequester,detail,email,subject) values (?,?,?,?,?,?,?)";
           System.out.println("FacilityMaintenanceDAO: ****************" + maintenanceRequestInsertQuery);
           maintenanceRequestImplStatement = con.prepareStatement(maintenanceRequestInsertQuery);
           
           maintenanceRequestImplStatement.setInt(1,maintenanceRequestImpl.getMaintenanceRequestId());
           //maintenanceRequestImplStatement.setString(2,DateUtil.getStringDateTime(maintenanceRequestImpl.getRequestDate()));
           //maintenanceRequestImplStatement.setDate(2,DateUtil.convertDateToSqlDate(maintenanceRequestImpl.getRequestDate()));
           maintenanceRequestImplStatement.setDate(2,new java.sql.Date(maintenanceRequestImpl.getRequestDate().getTime()));
           maintenanceRequestImplStatement.setInt(3, maintenanceRequestImpl.getFacilityImpl().getFacilityId());
           maintenanceRequestImplStatement.setString(4,maintenanceRequestImpl.getNameOfRequester());
           maintenanceRequestImplStatement.setString(5,maintenanceRequestImpl.getDetail());
           maintenanceRequestImplStatement.setString(6,maintenanceRequestImpl.getEmail());
           maintenanceRequestImplStatement.setString(7,maintenanceRequestImpl.getSubject());

           int mainReqInsertResult = maintenanceRequestImplStatement.executeUpdate();
           System.out.println(mainReqInsertResult + " MaintenanceRequestImpl Record Added from addFacilityMaintenanceImplRequest()");
           System.out.println("End of Query ***************: FacilityMaintenanceDAO");
    
           //manage resources 
           maintenanceRequestImplStatement.close();
           return true;
       }
       catch(Exception e){e.printStackTrace();}
       return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        Integer maintenanceRequestId = null;
        try {
            tx = session.beginTransaction();
            MaintenanceRequestImpl maintenanceRequest = new MaintenanceRequestImpl(facilityImpl, requestDate, nameOfRequester, detail, email, subject);
            maintenanceRequestId = (Integer) session.save(maintenanceRequest);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public boolean addScheduleMaintenanceMaintenanceRequestImpl(MaintenanceRequestImpl maintenanceRequestImpl, String workerName, MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl, Date startDateTime, Date endDateTime, MaintenanceCostImpl maintenanceCostImpl) {
        /*
        Connection con = conn.globalCon();
        String scheduleMaintenanceInsertQuery = "insert into Maintenance (maintenanceId,maintenanceRequestId,workerName,workerPhoneId,startDateTime,endDateTime,maintenanceCostId) values (?,?,?,?,?,?,?)";
        
        String table = "Maintenance";
        String row = "maintenanceId";
        String maintenanceImplExists = exist(table,row,maintenanceImpl.getMaintenanceId());
        if(maintenanceImplExists.equals("0"))
        {
            try
            {
                System.out.println("FacilityMaintenanceDAO: *************** " + scheduleMaintenanceInsertQuery);
                PreparedStatement maintenanceImplStatement = con.prepareStatement(scheduleMaintenanceInsertQuery);
                
                maintenanceImplStatement.setInt(1,maintenanceImpl.getMaintenanceId());
                maintenanceImplStatement.setInt(2,maintenanceImpl.getMaintenanceRequestImpl().getMaintenanceRequestId());
                maintenanceImplStatement.setString(3,maintenanceImpl.getWorkerName());
               //*************************************************8888
               //*************************************************8888
                MaintenanceWorkerPhoneImpl workerPhone = maintenanceImpl.getWorkerPhoneImpl();
                int workerPhoneId = workerPhone.getPhoneId();
                maintenanceImplStatement.setInt(4,workerPhoneId);
// maintenanceImplStatement.setString(4,maintenanceImpl.getWorkerPhone());
                //maintenanceImplStatement.setDate(5,DateUtil.getStringDateTime(maintenanceImpl.getStartDateTime()));
                maintenanceImplStatement.setDate(5,new java.sql.Date(maintenanceImpl.getStartDateTime().getTime()));
                maintenanceImplStatement.setDate(6, new java.sql.Date(maintenanceImpl.getEndDateTime().getTime()));
             maintenanceImplStatement.setInt(7,maintenanceImpl.getMaintenanceCostImpl().getMaintenanceCostId());
                
                int numberOfRowsAdded = maintenanceImplStatement.executeUpdate();
                System.out.println(numberOfRowsAdded + " MaintenanceImpl Record Added from addScheduleMaintenance()");
                System.out.println("End of Query ***************: FacilityMaintenanceDAO");
                return true;
            }
            catch(Exception e){}
        }
        else
            System.out.println("MaintenanceImpl Record already exists from addFacilityMaintenanceImplRequest()");
        return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        MaintenanceImpl maintenance = null;
        Integer maintenanceId = null;
        try {
            tx = session.beginTransaction();
            maintenance = new MaintenanceImpl(maintenanceRequestImpl, workerName, maintenanceWorkerPhoneImpl, startDateTime, endDateTime, maintenanceCostImpl);
            maintenanceId = (Integer) session.save(maintenance);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public boolean calcMaintenanceCostForFacility(MaintenanceImpl maintenanceImpl, Integer materialCost, BigDecimal laborCost, BigDecimal totalCost, String isPaid) {
        /*
        Connection con = conn.globalCon();
        
        String maintenanceCostImplTable = "MaintenanceCost";
        String maintenanceCostImplRow= "maintenanceCostId";
        String maintenanceCostImplExists = exist(maintenanceCostImplTable,maintenanceCostImplRow,maintenanceCostImpl.getMaintenanceCostId());
        if(maintenanceCostImplExists.equals("0"))
        {
            try 
            {
                PreparedStatement maintenanceCostImplStatement = null;
                String maintenanceCostImplInsertQuery = "insert into MaintenanceCost (maintenanceCostId,materialCost,laborCost,totalCost,isPaid) values(?,?,?,?,?)";
                System.out.println("FacilityMaintenanceDAO:****************" + maintenanceCostImplInsertQuery);
                maintenanceCostImplStatement = con.prepareStatement(maintenanceCostImplInsertQuery);
                
                maintenanceCostImplStatement.setInt(1,maintenanceCostImpl.getMaintenanceCostId());
                maintenanceCostImplStatement.setInt(2,maintenanceCostImpl.getMaterialCost());
                maintenanceCostImplStatement.setBigDecimal(3,maintenanceCostImpl.getLaborCost());
                maintenanceCostImplStatement.setBigDecimal(4,maintenanceCostImpl.getTotalCost());
                maintenanceCostImplStatement.setString(5,maintenanceCostImpl.getIsPaid());
                int numberOfRowsAdded = maintenanceCostImplStatement.executeUpdate();
                
                System.out.println(numberOfRowsAdded + " MaintenanceCostImpl Record Added from calcMaintenanceCostForFacility()");
                System.out.println("End of Query***************: FacilityMaintenanceDAO");
            } 
            catch (Exception e) {e.printStackTrace();}
        }
        return false;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        Integer maintenanceCostId = null;
        try {
            tx = session.beginTransaction();
            MaintenanceCostImpl maintenanceCost = new MaintenanceCostImpl(maintenanceImpl, materialCost, laborCost, totalCost, isPaid);
            maintenanceCostId = (Integer) session.save(maintenanceCost);
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public List<MaintenanceRequestImpl> listMaintRequests() {
        /*
        Connection con = conn.globalCon();
        List<MaintenanceRequestImpl> maintenanceRequestImplList = new ArrayList<MaintenanceRequestImpl>();
        try 
        {
            Statement st = con.createStatement();
            String sql = "select maintenanceRequestId,facilityId,requestDate,nameOfRequester,detail,email,subject from MaintenanceRequest";
            
            ResultSet rs = st.executeQuery(sql);
            if (rs.next() == false) 
            {
                System.out.println("No MaintenanceRequestsImpl Record exists");
            }
            else
            {
                rs.beforeFirst();
                System.out.println("FacilityMaintenanceDAO: ***************" + sql);
                int numOfRowsSaved = 0;
                int facilityId = numOfRowsSaved;
                MaintenanceRequestImpl maintenanceRequestImpl = new MaintenanceRequestImpl();
                FacilityImpl facilityImpl = null;
                FacilityAddressImpl facilityAddressImpl = null;
                while (rs.next()) {
                    maintenanceRequestImpl.setMaintenanceRequestId(rs.getInt("maintenanceRequestId"));
                    facilityId = rs.getInt("facilityId");
                    
                    facilityImpl = this.getFacilityImplFromId(facilityId);
                    facilityImpl.setFacilityId(facilityId);
                    
                    facilityAddressImpl = facilityImpl.getFacilityAddressImpl();
                    int facilityAddressId = facilityAddressImpl.getFacilityAddressId(); 
                    facilityAddressImpl = this.getFacilityAddressImplFromId(facilityAddressId);
                    facilityImpl.setFacilityAddressImpl(facilityAddressImpl);
                    
                    maintenanceRequestImpl.setFacilityImpl(facilityImpl);
                    maintenanceRequestImpl.setRequestDate(rs.getDate("requestDate"));
                    maintenanceRequestImpl.setDetail(rs.getString("detail"));
                    maintenanceRequestImpl.setEmail(rs.getString("email"));
                    maintenanceRequestImpl.setSubject(rs.getString("subject"));
                    numOfRowsSaved++;
                }
                //start of getting facility for MaintenanceRequest
               
                System.out.println(numOfRowsSaved + "MaintenanceRequestImpl Records saved from listMaintRequests()");
                System.out.println("End of Query ***************: FacilityMaintenanceDAO");
                return maintenanceRequestImplList;
            }
        }
        catch(Exception e)
        {}
        return null;
         */

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenanceRequests = session.createQuery("FROM MaintenanceRequest").list();
            List<MaintenanceRequestImpl> maintenanceRequestList = new ArrayList<MaintenanceRequestImpl>();
            for (Iterator iterator
                    = maintenanceRequests.iterator(); iterator.hasNext();) {
                MaintenanceRequestImpl maintenanceRequest = (MaintenanceRequestImpl) iterator.next();
                maintenanceRequestList.add(maintenanceRequest);
            }
            tx.commit();
            return maintenanceRequestList;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public FacilityImpl getFacilityImplFromId(int facilityId) {
        /*Connection con = conn.globalCon();
        String facilityImplExists = exist("Facility", "facilityId", facilityId);
        if (facilityImplExists.equals("0")) {
            System.out.println("FacilityImpl Record " + "Number: " + facilityId +" Does Not Exist from getFacilityImplFromId()");
            return null;
        }
        else
        {
            try
            {
            FacilityImpl facilityImpl = new FacilityImpl();
            String facilitySelectQuery = "Select facilityAddressId,facilityName,capacity,totalArea,facilityType from Facility where facilityId='" + facilityId + "'";
            Statement facilityImplStatement = con.createStatement();
            ResultSet facilitySelectResultSet = facilityImplStatement.executeQuery(facilitySelectQuery);
            
            while(facilitySelectResultSet.next())
            {
                facilityImpl.setFacilityId(facilityId);
                FacilityAddressImpl facilityAddressImpl = new FacilityAddressImpl();
                
                int facilityAddressId = facilitySelectResultSet.getInt("facilityAddressId");
                facilityAddressImpl = this.getFacilityAddressImplFromId(facilityAddressId);
                facilityImpl.setFacilityAddressImpl(facilityAddressImpl);
                facilityImpl.setFacilityName(facilitySelectResultSet.getString("facilityName"));
                facilityImpl.setCapacity(facilitySelectResultSet.getInt("capacity"));
                facilityImpl.setTotalArea(facilitySelectResultSet.getBigDecimal("totalArea"));
                facilityImpl.setFacilityType(facilitySelectResultSet.getString("facilityType"));
                
            }
            facilitySelectResultSet.close();
            facilityImplStatement.close();
            return facilityImpl;
            }
            catch(Exception e)
            {}
        }
        return null;*/
        Session session = factory.openSession();
        Transaction tx = null;
        FacilityImpl facility = null;
        try {
            tx = session.beginTransaction();
            List facilityList = session.createQuery("FROM Facility WHERE facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = facilityList.iterator(); iterator.hasNext();) {
                facility = (FacilityImpl) iterator.next();
            }
            tx.commit();
            return facility;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public FacilityAddressImpl getFacilityAddressImplFromId(int facilityAddressId) {
        /*
        Connection con = conn.globalCon();
        String facilityAddressExists = exist("FacilityAddress","facilityAddressId",facilityAddressId);
        if(facilityAddressExists.equals("0"))
        {
            System.out.println("FacilityAddressImpl Record does not exist from getFacilityAddressImplFromId()");
        }
        else
        {
            try
            {
                FacilityAddressImpl facilityAddressImpl = new FacilityAddressImpl();
                String facilityAddressSelectQuery = "select streetAddress, city,state,zipcode from FacilityAddress where facilityAddressId ='" + facilityAddressId + "'";
                Statement facilityAddressStatement = con.createStatement();
                ResultSet facilityAddressResultSet = facilityAddressStatement.executeQuery(facilityAddressSelectQuery);
                System.out.println("FacilityMaintenanceDAO:***************"+facilityAddressSelectQuery);
                while(facilityAddressResultSet.next())
                {
                    facilityAddressImpl.setFacilityAddressId(facilityAddressId);
                    facilityAddressImpl.setStreetAddress(facilityAddressResultSet.getString("streetAddress"));
                    facilityAddressImpl.setCity(facilityAddressResultSet.getString("city"));
                    facilityAddressImpl.setState(facilityAddressResultSet.getString("state"));
                    facilityAddressImpl.setZipCode(facilityAddressResultSet.getString("zipcode"));
                    
                    System.out.println("1 FacilityAddress Record Selected from getFacilityAddressImplFromId()");
                    
                }
                facilityAddressStatement.close();
                facilityAddressResultSet.close();
                System.out.println("End of Query**************** :FacilityMaintenanceDAO");
                return facilityAddressImpl;
            }
            catch(Exception e){};
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        FacilityAddressImpl facilityAddress = null;
        try {
            tx = session.beginTransaction();
            List facilityAddressList = session.createQuery("FROM FacilityAddress").list();
            for (Iterator iterator
                    = facilityAddressList.iterator(); iterator.hasNext();) {
                facilityAddress = (FacilityAddressImpl) iterator.next();
                facilityAddressList.add(facilityAddress);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return facilityAddress;
    }

    public MaintenanceCostImpl getMaintenanceCostImplFromId(int maintenanceCostId) {
        /*
        Connection con = conn.globalCon();
        String maintenanceCostExists = exist("MaintenanceCost","maintenanceCostId", maintenanceCostId);
        String maintenanceCostSelectQuery = "select MaintenanceCostId, materialCost,laborCost,totalCost,isPaid, maintenanceId from MaintenanceCost";
        System.out.println("FacilityMaintenanceDAO:****************"+maintenanceCostSelectQuery);
        if(maintenanceCostExists.equals("0"))
        {
            System.out.println("No MaintenanceCostImpl Record exists with Id "+maintenanceCostId);
            System.out.println("End of Query: FacilityMaintenanceDAO: *******************");
        }
        
        else
        {
            try
            {
                Statement maintenanceCostStatement = con.createStatement();
                ResultSet maintenanceCostResultSet = maintenanceCostStatement.executeQuery(maintenanceCostSelectQuery);
                MaintenanceCostImpl maintenanceCostImpl = new MaintenanceCostImpl();
                MaintenanceImpl maintenanceImpl = null;
                while(maintenanceCostResultSet.next())
                {
                    maintenanceCostImpl.setMaintenanceCostId(maintenanceCostId);
                    maintenanceCostImpl.setMaterialCost(maintenanceCostResultSet.getInt("materialCost"));
                    maintenanceCostImpl.setLaborCost(maintenanceCostResultSet.getBigDecimal("laborCost"));
                    maintenanceCostImpl.setTotalCost(maintenanceCostResultSet.getBigDecimal("totalCost"));
                    maintenanceCostImpl.setIsPaid(maintenanceCostResultSet.getString("isPaid"));
                    
                    int maintenanceImplId = maintenanceCostResultSet.getInt("maintenanceId");
                    
                    maintenanceImpl = this.getMaintenanceImplFromId(maintenanceImplId);
                    maintenanceCostImpl.setMaintenanceImpl(maintenanceImpl);
                }
                System.out.println("1 MaintenanceCostImpl Record Selected from getMaintenanceCostImplFromId()");
                System.out.println("End of Query ***************: FacilityMaintenanceDAO");
                
                maintenanceCostStatement.close();
                maintenanceCostResultSet.close();
                return maintenanceCostImpl;
            }
            catch(Exception e)
            {}
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        MaintenanceCostImpl maintenanceCost = null;
        try {
            tx = session.beginTransaction();
            List maintenanceCostList = session.createQuery("FROM MaintenanceCost WHERE maintenance_Cost_Id ='" + maintenanceCostId + "'").list();
            for (Iterator iterator
                    = maintenanceCostList.iterator(); iterator.hasNext();) {
                maintenanceCost = (MaintenanceCostImpl) iterator.next();
            }
            tx.commit();
            return maintenanceCost;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public MaintenanceImpl getMaintenanceImplFromId(int maintenanceId) {
        /*
        Connection con = conn.globalCon();
        String maintenanceImplExists = exist("Maintenance","maintenanceId",maintenanceId);
        if(maintenanceImplExists.equals("0"))
            System.out.println("No MaintenanceImpl Record exists from getMaintenanceImplFromId()");
        else
        {
            try
            {
              MaintenanceImpl maintenance = new MaintenanceImpl();
              Statement maintenanceStatement = con.createStatement();
              String maintenanceSelectQuery="select workerName,workerPhoneId,startDateTime,endDateTime,maintenanceCostId,maintenanceRequestId from Maintenance where maintenanceId='" + maintenanceId +"'";
              ResultSet maintenanceResultSet = maintenanceStatement.executeQuery(maintenanceSelectQuery);
            
              System.out.println("FacilityMaintenanceDAO:***************"+maintenanceSelectQuery);
              MaintenanceCostImpl maintenanceCostImpl = null;
              int maintenanceCostId = 0;
              int maintenanceRequestId = 0;
              MaintenanceRequestImpl maintenanceRequestImpl = null;
              while(maintenanceResultSet.next())
              {
                  maintenance.setMaintenanceId(maintenanceId);
                  maintenance.setWorkerName(maintenanceResultSet.getString("workerName"));
                  
                  int workerPhoneId = maintenanceResultSet.getInt("workerPhoneId");
                  MaintenanceWorkerPhoneImpl workerPhoneImpl = this.getMaintenanceWorkerPhoneImplFromId(workerPhoneId);
                  
                  maintenance.setWorkerPhoneImpl(workerPhoneImpl);
                  maintenance.setStartDateTime(maintenanceResultSet.getDate("startDateTime"));
                  maintenance.setEndDateTime(maintenanceResultSet.getDate("endDateTime"));
                  
                  maintenanceCostId = maintenanceResultSet.getInt("maintenanceCostId");
                  maintenanceCostImpl = this.getMaintenanceCostImplFromId(maintenanceCostId);
                  maintenance.setMaintenanceCostImpl(maintenanceCostImpl);
                  
                  maintenanceRequestId = maintenanceResultSet.getInt("maintenanceRequestId");
                  maintenanceRequestImpl = this.getMaintenanceRequestImplFromId(maintenanceRequestId);
                  maintenance.setMaintenanceRequestImpl(maintenanceRequestImpl);
              }
              System.out.println("1 Maintenance Record Selected from getMaintenanceImplFromId()");
              System.out.println("End of Query ************: FacilityMaintenanceDAO");
              maintenanceStatement.close();
              maintenanceResultSet.close();
              return maintenance; 
            }
            catch(Exception e)
            {}
        }
        return null;
         */
        MaintenanceImpl maintenance = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenanceList = session.createQuery("FROM Maintenance where maintenance_Id='" + maintenanceId + "'").list();
            for (Iterator iterator
                    = maintenanceList.iterator(); iterator.hasNext();) {
                maintenance = (MaintenanceImpl) iterator.next();
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return maintenance;
    }

    public MaintenanceRequestImpl getMaintenanceRequestImplFromId(int maintenanceRequestId) {
        /*
        Connection con = conn.globalCon();
        String maintenanceRequestImplExists = exist("MaintenanceRequest","maintenanceRequestId",maintenanceRequestId);
        
        if(maintenanceRequestImplExists.equals("0"))
        {
            System.out.println("No MaintenanceRequestImpl Record found from getMaintenanceRequestImplFromId()");
        }
        else
        {
            MaintenanceRequestImpl maintenanceRequestImpl = null;
            try
            {
                String maintenanceRequestSelect ="select maintenanceRequestId,facilityId,requestDate,nameOfRequester,detail,email,subject from MaintenanceRequest where maintenanceRequestId='"+maintenanceRequestId +"'";
                Statement maintenanceRequestStatement = con.createStatement();
                ResultSet maintenanceRequestResultSet = maintenanceRequestStatement.executeQuery(maintenanceRequestSelect);
                FacilityImpl facilityImpl = null;
                while(maintenanceRequestResultSet.next())
                {
                    maintenanceRequestImpl = new MaintenanceRequestImpl();
                    maintenanceRequestImpl.setMaintenanceRequestId(maintenanceRequestId);
                    
                    int facilityImplId = maintenanceRequestResultSet.getInt("facilityId");
                    facilityImpl = this.getFacilityImplFromId(facilityImplId);
                    maintenanceRequestImpl.setFacilityImpl(facilityImpl);
                    maintenanceRequestImpl.setRequestDate(maintenanceRequestResultSet.getDate("requestDate"));
                    maintenanceRequestImpl.setNameOfRequester(maintenanceRequestResultSet.getString("nameOfRequester"));
                    maintenanceRequestImpl.setDetail(maintenanceRequestResultSet.getString("detail"));
                    maintenanceRequestImpl.setEmail(maintenanceRequestResultSet.getString("email"));
                    maintenanceRequestImpl.setSubject(maintenanceRequestResultSet.getString("subject"));
                }
            }
            catch(Exception e)
            {}
        }
        return null;
         */
        MaintenanceRequestImpl maintenanceRequest = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenanceRequestList = session.createQuery("FROM MaintenanceRequest WHERE maintenance_Request_Id ='" + maintenanceRequestId + "'").list();
            for (Iterator iterator
                    = maintenanceRequestList.iterator(); iterator.hasNext();) {
                maintenanceRequest = (MaintenanceRequestImpl) iterator.next();
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return maintenanceRequest;
    }

    public List<MaintenanceImpl> listMaintenance() {
        /*
        Connection con = conn.globalCon();
        String maintenanceSelectQuery = "select maintenanceId, workerName,workerPhone,startDateTime,endDateTime,maintenanceCostId,maintenanceRequestId from Maintenance";
        List<MaintenanceImpl> maintenanceArrayList = new ArrayList<MaintenanceImpl>();
        
         try 
        {
            Statement maintenanceStatement = con.createStatement();            
            ResultSet maintenanceResultSet = maintenanceStatement.executeQuery(maintenanceSelectQuery);
            if (maintenanceResultSet.next() == false) 
            {
                System.out.println("No MaintenanceRequestsImpl Record exists");
            }
            else
            {
                maintenanceResultSet.beforeFirst();
                System.out.println("FacilityMaintenanceDAO: ***************" + maintenanceSelectQuery);
                int numberOfRowsSelected = 0;
                while (maintenanceResultSet.next()) {

                    int maintenanceImplId = maintenanceResultSet.getInt("maintenanceId");
                    MaintenanceImpl maintenance = new MaintenanceImpl();
                    maintenance.setMaintenanceId(maintenanceImplId);
                    maintenance.setWorkerName(maintenanceResultSet.getString("workerName"));

                    int workerPhoneId = maintenanceResultSet.getInt("workerPhoneId");
                    MaintenanceWorkerPhoneImpl workerPhoneImpl = this.getMaintenanceWorkerPhoneImplFromId(workerPhoneId);

                    maintenance.setWorkerPhoneImpl(workerPhoneImpl);
                    maintenance.setStartDateTime(maintenanceResultSet.getDate("startDateTime"));
                    maintenance.setEndDateTime(maintenanceResultSet.getDate("endDateTime"));
                    MaintenanceCostImpl maintenanceCostImpl = new MaintenanceCostImpl();
                    int maintenanceCostId = maintenanceResultSet.getInt("maintenanceCostId");
                    maintenanceCostImpl = this.getMaintenanceCostImplFromId(maintenanceCostId);
                    maintenance.setMaintenanceCostImpl(maintenanceCostImpl);

                    MaintenanceRequestImpl maintenanceRequestImpl = new MaintenanceRequestImpl();
                    int maintenanceRequestId = maintenanceResultSet.getInt("maintenanceRequestId");
                    maintenanceRequestImpl = this.getMaintenanceRequestImplFromId(maintenanceRequestId);
                    maintenance.setMaintenanceRequestImpl(maintenanceRequestImpl);
                    maintenanceArrayList.add(maintenance);
                    numberOfRowsSelected++;

                }
                System.out.println(numberOfRowsSelected + " Maintenance Records Selected from listMaintenance()");
                System.out.println("End of Query ***************: FacilityMaintenanceDAO");
                return maintenanceArrayList;
            }
        }
        catch(Exception e)
        {}
         return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<MaintenanceImpl> maintenanceList = new ArrayList<MaintenanceImpl>();
            List maintenances = session.createQuery("from Maintenance").list();
            for (Iterator iterator
                    = maintenances.iterator(); iterator.hasNext();) {
                MaintenanceImpl maintenance = (MaintenanceImpl) iterator.next();
                maintenanceList.add(maintenance);
            }
            tx.commit();
            return maintenanceList;

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public long calcProblemRateForFacility(int facilityId) {//problemRate procured from maintenanceRequest
        /*
        MaintenanceRequestImpl maintenanceRequestImpl = this.getMaintenanceRequestImplFromFacilityId(facilityId);
        List<Date> maintenanceRequestDates = this.listMaintenanceRequestImplDates();
        long timeMaintenanceRequestDays= 0;
        
        for(int i = 0; i< maintenanceRequestDates.size();i++)
        {
            timeMaintenanceRequestDays += 5;
        }
        
                System.out.println("Problem rate: " + timeMaintenanceRequestDays + "%");
        return timeMaintenanceRequestDays;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            long problemR = 0;
            List maintenanceRequests = session.createQuery("SELECT request_Date FROM MaintenanceRequest WHERE facility_Id ='" + facilityId + "'").list();
            List<Date> problemDays = new ArrayList<Date>();
            for (Iterator iterator
                    = maintenanceRequests.iterator(); iterator.hasNext();) {
                MaintenanceRequestImpl maintenanceRequest = (MaintenanceRequestImpl) iterator.next();
                problemDays.add(maintenanceRequest.getRequestDate());
            }
            tx.commit();
            for (Date problemDayRequest : problemDays) {
                problemR += 5;
            }
            return problemR;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return (long)0;
    }

    public int calcDownTimeForFacility(int facilityId) {
        /*
        Connection con = conn.globalCon();
        //this function adds the total startDAte and endDate 
        String maintenanceDatesSelectQuery = "SELECT m.startDateTime, m.endDateTime FROM Maintenance AS m, MaintenanceRequest, MaintenanceRequest AS mr WHERE mr.maintenanceRequestId= m.maintenanceRequestId;";
        try
        {
            Statement maintenanceDatesStatement = con.createStatement();
        
        ResultSet maintenanceDateResultSet = maintenanceDatesStatement.getResultSet();
        if(maintenanceDateResultSet.next() == false)
        {
            System.out.println("Maintenance Reports clear");
            return 0; 
        }
        else
        {
            int hours = 0;
            maintenanceDateResultSet.beforeFirst();
            
            while(maintenanceDateResultSet.next())
            {
             Date start = maintenanceDateResultSet.getDate("startDateTime");
             Date end = maintenanceDateResultSet.getDate("endDateTime");
             
             long endTime = end.getTime();
             long startTime = start.getTime();
             long remainingHours = endTime - startTime;
             
             hours += remainingHours;
             
            }
            
            
            System.out.println("Facility Maintenance has downtime : " + hours + " hours");
            System.out.println("End of query *************** :FacilityMaintenanceDAO");
            maintenanceDatesStatement.close();
            maintenanceDateResultSet.close();
            con.close();
            return hours;
        }        
        }
        catch(Exception e)
        {}
        return 0;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenances = session.createQuery("FROM Maintenance").list();
            List<Date> startDates = new ArrayList<Date>();
            List<Date> endDates = new ArrayList<Date>();
            for (Iterator iterator
                    = maintenances.iterator(); iterator.hasNext();) {
                MaintenanceImpl maintenance = (MaintenanceImpl) iterator.next();
                startDates.add(maintenance.getStartDateTime());
                endDates.add(maintenance.getEndDateTime());
            }
            tx.commit();
            int hours = 0;
            for (int ii = 0; ii < startDates.size(); ii++) {
                long earliest = startDates.get(ii).getTime();
                long latest = endDates.get(ii).getTime();
                long remaining = latest - earliest;
                hours += remaining;
            }
            return hours;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return 0;
    }

    public List<Date> listMaintenanceRequestImplDates() {
        /*
        List<Date> maintenanceRequestImplDateList = new ArrayList<Date>();
        Connection con = conn.globalCon();
        try
        {
            String maintenanceRequestImplDateSelectQuery = "select requestDate from MaintenanceRequest";
            Statement maintenanceRequestImplStatement = con.createStatement();
            ResultSet maintenanceRequestImplResultSet =  maintenanceRequestImplStatement.executeQuery(maintenanceRequestImplDateSelectQuery);
            System.out.println("FacilityMaintenanceDAO:***************"+maintenanceRequestImplDateSelectQuery);
            int numberOfDatesSelected = 0;
            while(maintenanceRequestImplResultSet.next())
            {
                
                Date date = null;
                date = maintenanceRequestImplResultSet.getDate("requestDate");
                maintenanceRequestImplDateList.add(date);
                numberOfDatesSelected++;
            }
            System.out.println(numberOfDatesSelected + "MaintenanceRequest Dates selected");
            System.out.println("End of Query ****************: FacilityMaintenanceDAO");
            maintenanceRequestImplStatement.close();
            maintenanceRequestImplResultSet.close();
            con.close();
            return maintenanceRequestImplDateList;  
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List maintenanceRequestList = session.createQuery("FROM MaintenanceRequest").list();
            List<Date> maintRequestDates = new ArrayList<Date>();
            for (Iterator iterator
                    = maintenanceRequestList.iterator(); iterator.hasNext();) {
                MaintenanceRequestImpl maintenanceRequest = (MaintenanceRequestImpl) iterator.next();
                maintRequestDates.add(maintenanceRequest.getRequestDate());
            }
            tx.commit();
            return maintRequestDates;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public MaintenanceRequestImpl getMaintenanceRequestImplFromFacilityId(int facilityId) {
        /*
        Connection con = conn.globalCon();
        String maintenanceRequestImplExists = exist("MaintenanceRequest","facilityId",facilityId);
        if(maintenanceRequestImplExists.equals("0"))
        {
            System.out.println("No MaintenanceRequest Record found");       
        }
        else 
        {
            try 
            {
                String maintenanceRequestSelectQuery = "select maintenanceRequestId, requestDate,nameOfRequester,detail,email,subject from MaintenanceRequest where facilityId ='" + facilityId + "'";
                Statement maintenanceRequestStatement = con.createStatement();
                ResultSet maintenanceRequestResultSet = maintenanceRequestStatement.executeQuery(maintenanceRequestSelectQuery);
                MaintenanceRequestImpl mr = new MaintenanceRequestImpl();
                FacilityImpl fi = new FacilityImpl();
                while(maintenanceRequestResultSet.next())
                {
                    System.out.println("FacilityMaintenanceDAO:****************"+maintenanceRequestSelectQuery);
                    mr.setMaintenanceRequestId(maintenanceRequestResultSet.getInt("maintenanceRequestId"));
                    mr.setRequestDate(maintenanceRequestResultSet.getDate("requestDate"));
                    mr.setNameOfRequester(maintenanceRequestResultSet.getString("nameOfRequester"));
                    mr.setDetail(maintenanceRequestResultSet.getString("detail"));
                    mr.setEmail(maintenanceRequestResultSet.getString("email"));
                    mr.setSubject(maintenanceRequestResultSet.getString("subject"));
                    fi = this.getFacilityImplFromId(facilityId);
                    mr.setFacilityImpl(fi);
                    System.out.println("1 MaintenanceRequestImpl Record Selected from getMaintenanceRequestImplFromFacilityId()");
                    System.out.println("End of Query ***************: FacilityMaintenanceDAO");
                }
                maintenanceRequestResultSet.close();
                maintenanceRequestStatement.close();
                con.close();
            } 
            catch (Exception e) 
            {}
        }
        return null; 
         */
        Session session = factory.openSession();
        Transaction tx = null;
        MaintenanceRequestImpl maintenanceRequest = null;
        try {
            tx = session.beginTransaction();
            List maintenanceRequestList = session.createQuery("FROM MaintenanceRequest WHERE facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator = maintenanceRequestList.iterator(); iterator.hasNext();) {
                maintenanceRequest = (MaintenanceRequestImpl) iterator.next();
            }
            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return maintenanceRequest;
    }

    public List<String> getListOfFacilityProblems(int facilityId) {
        /*
        Connection con = conn.globalCon();
        String exist = exist("MaintenanceRequest","facilityId",facilityId);
        if(exist.equals("0"))
        {
            System.out.println("Facility Record not found");
        }
        else
        {
            try
            {
              String sql = "select detail from MaintenanceRequest where facilityId ='" + facilityId +"'";
              Statement st = con.createStatement();
              ResultSet rs = st.executeQuery(sql);
              List<String> facilityProblemList = new ArrayList<String>();
              System.out.println("FacilityMaintenanceDAO:*************"  + sql);
              int numberOfRowsSelected = 0;
              while(rs.next())
              {
                  numberOfRowsSelected++;
                  String detail = rs.getString("detail");
                  facilityProblemList.add(detail);
              }
              rs.close();
              st.close();
              con.close();
              System.out.println(numberOfRowsSelected + "Facility Problems added");
              System.out.println("End of Query ****************: FacilityMaintenanceDAO");
              return facilityProblemList;
            }
            catch(Exception e)
            {}
        }
        return null;
         */
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<String> listOfFacilityProblems = new ArrayList<String>();
            List problems = session.createQuery("SELECT detail from MaintenanceRequest where facility_Id ='" + facilityId + "'").list();
            for (Iterator iterator
                    = problems.iterator(); iterator.hasNext();) {
                String problem = (String) iterator.next();
                listOfFacilityProblems.add(problem);
            }
            return listOfFacilityProblems;
        } catch (HibernateException e) {
            if (/*e*/tx != null)/*e*/ {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public static void main(String[] args) {
        MaintenanceRequestImpl maint = new MaintenanceRequestImpl();
        maint.setMaintenanceRequestId(99);
        maint.setNameOfRequester("bob");
        maint.setDetail("");
        maint.setSubject("");
        maint.setEmail("");
        maint.setRequestDate(new Date());

        FacilityImpl fa = new FacilityImpl();
        fa.setFacilityId(234);
        maint.setFacilityImpl(fa);

        MaintenanceImpl maintenanceImpl = new MaintenanceImpl();
        maintenanceImpl.setMaintenanceId(9);
        maintenanceImpl.setWorkerName("Shanazzi");
        MaintenanceWorkerPhoneImpl workerPhoneImpl = new MaintenanceWorkerPhoneImpl();
        workerPhoneImpl.setPhoneId(9);
        workerPhoneImpl.setPhoneCarrier(MaintenancePhoneCarrier.ATT);
        workerPhoneImpl.setPhoneNumber("123456789");
        workerPhoneImpl.setPhoneType(MaintenancePhoneType.FLIP);
        maintenanceImpl.setWorkerPhoneImpl(workerPhoneImpl);
        maintenanceImpl.setEndDateTime(new Date());
        maintenanceImpl.setStartDateTime(new Date());

        MaintenanceCostImpl maintenanceCostImpl = new MaintenanceCostImpl();
        maintenanceCostImpl.setIsPaid("Yes");
        maintenanceCostImpl.setLaborCost(BigDecimal.TEN);
        maintenanceCostImpl.setMaintenanceImpl(maintenanceImpl);
        maintenanceCostImpl.setMaintenanceCostId(99);
        maintenanceCostImpl.setMaterialCost(20000);
        maintenanceImpl.setMaintenanceCostImpl(maintenanceCostImpl);

        MaintenanceRequestImpl maintenanceRequestImpl = new MaintenanceRequestImpl();
        maintenanceRequestImpl.setMaintenanceRequestId(27);
        maintenanceRequestImpl.setEmail("@@@");
        maintenanceRequestImpl.setDetail("slow");
        maintenanceRequestImpl.setFacilityImpl(fa);
        maintenanceRequestImpl.setRequestDate(new Date());
        maintenanceImpl.setMaintenanceRequestImpl(maintenanceRequestImpl);
        FacilityMaintenanceDAO mf = new FacilityMaintenanceDAO();
        //mf.addFacilityMaintenanceImplRequest(maintenanceRequestImpl);
        FacilityAddressImpl facilityAddressImpl = new FacilityAddressImpl();
        facilityAddressImpl.setFacilityAddressId(9);
        facilityAddressImpl.setCity("Morton");
        facilityAddressImpl.setState("Illinois");
        facilityAddressImpl.setStreetAddress("ABC123");
        facilityAddressImpl.setZipCode("60606");

        FacilityImpl facilityImpl = new FacilityImpl();
        facilityImpl.setFacilityId(234);
        facilityImpl.setFacilityAddressImpl(facilityAddressImpl);
        facilityImpl.setFacilityName("JOHN");
        facilityImpl.setCapacity(9);
        facilityImpl.setTotalArea(BigDecimal.ONE);
        facilityImpl.setFacilityType("BUILDING");
        //List<MaintenanceRequestImpl> maintenanceRequestArrayList = mf.listMaintRequests();
        //mf.calcMaintenanceCostForFacility(maintenanceCostImpl);
        //FacilityAddressImpl temporary = mf.getFacilityAddressImplFromId(9);
        //List<MaintenanceImpl> maintenanceArrayList = mf.listMaintenance();
        // mf.listMaintenanceRequestImplDates();

        long x = mf.calcProblemRateForFacility(9);
        //System.out.println(9);
        // System.out.println(mf.calcDownTimeForFacility(9);

    }
    ApplicationContext context = new ClassPathXmlApplicationContext("facility.management.dal.daos.xml");
    DBHelper conn = (DBHelper) context.getBean("dBHelper");//new DBHelper();
    private static SessionFactory factory;
}
