/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.client;

import facility.management.model.facility.BuildingImpl;
import facility.management.model.facility.BuildingUnitImpl;
import facility.management.model.facility.FacilityAddressImpl;
import facility.management.model.facility.FacilityImpl;
import facility.management.model.maintenance.MaintenanceCostImpl;
import facility.management.model.maintenance.MaintenanceImpl;
import facility.management.model.maintenance.MaintenanceRequestImpl;
import facility.management.model.maintenance.MaintenanceWorkerPhoneImpl;
import facility.management.model.useage.InspectionImpl;
import facility.management.model.useage.LeaseInfoImpl;
import facility.management.model.useage.LeasePaymentImpl;
import facility.management.model.useage.OwnerImpl;
import facility.management.model.useage.PurchaseInfoImpl;
import facility.management.model.useage.TenantImpl;
import facility.management.service.impl.FacilityMaintenanceServiceImpl;
import facility.management.service.impl.FacilityServiceImpl;
import facility.management.service.impl.FacilityUsageServiceImpl;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.security.auth.login.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.hibernate.SessionFactory;
/**
 *
 * @author dmartin3
 */
public class FacilityClient {
    
    private static void printFacilityProblems(int facilityId)
    {
        List<String> problemList = facilityMaintenanceService.listFacilityProblems(facilityId);
        System.out.println("Facility : " + facilityId + "has these problems: ");
        for(String problem : problemList)
            System.out.println(problem);
   
    }
    private static void printDownTimeForFacility(int facilityId)
    {
        long downTime = facilityMaintenanceService.calcDownTimeForFacility(facilityId);
        System.out.println("Down Time for Facility:" + downTime);
    }
    private static void printProblemRateForFacility(int facilityId)
    {
        long temp =  facilityMaintenanceService.calcProblemRateForFacility(facilityId);
        System.out.println("Problem rate for Facility: " + temp);
    }
    private static void printFacilityMaintenanceRequest(MaintenanceRequestImpl maintenanceRequestImpl)
    {
        facilityMaintenanceService.makeFacilityMaintRequest(maintenanceRequestImpl);
    }
    private static void printScheduleMaintenance(MaintenanceImpl maintenanceImpl)
    {
        facilityMaintenanceService.scheduleMaintenance(maintenanceImpl);
    }
    private static void printMaintenanceCostForFacility(MaintenanceCostImpl maintenanceCostImpl)
    {
        facilityMaintenanceService.calcMaintenanceCostForFacility(maintenanceCostImpl);
    }
    private static void printListMaintRequests()
    {
        List<MaintenanceRequestImpl> maintRequestsList = facilityMaintenanceService.listMaintRequests();
        for(MaintenanceRequestImpl maintenanceRequest : maintRequestsList)
        {
            maintenanceRequest.toString2();
        }
    }
    
    public static void printListMaintenance()
    {
        List<MaintenanceImpl> maintenanceList = facilityMaintenanceService.listMaintenance();
        for(MaintenanceImpl maintenance : maintenanceList)
        {
            maintenance.toString2();
        }
    }
    public static void printListFacilities()
    {
        List<FacilityImpl> facilityList = facilityService.listFacilities();
        for(FacilityImpl facility : facilityList)
        {
            facility.toString2();
        }
    }
    public static void printFacilityInformation(int facilityId)
    {
        FacilityImpl facility = facilityService.getFacilityInformation(facilityId);
        facility.toString2();
    }
    public static void printAvailableCapacity(int facilityId)
    {
        Long capacity = facilityService.requestAvailableCapacity(facilityId);
        System.out.println("Capacity: " + capacity);
    }
    public static void printAddFacility(FacilityImpl facility)
    {
        boolean test = facilityService.addNewFacility(facility);
        System.out.println("Add facility worked: " + test);
    }
    public static void printAddFacilityDetail(BuildingImpl building)
    {
        try
        {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer buildingId = (Integer) session.save(building);
        tx.commit();
        }
        catch(HibernateException e)
        {
            if(tx!=null) tx.rollback();
            e.printStackTrace();
        }
        facilityService.addFacilityDetail(building);
        System.out.println("Building facility added");
    }
    public static void printRemoveFacility(int facilityId)
    {
        boolean test = facilityService.removeFacility(facilityId);
        System.out.println(test + " Facility was removed");
    }
    public static void printIsInUseDuringInterval(int facilityId, Date sDate, Date eDate)
    {
        boolean test = facilityUsageService.isInUseDuringInterval(facilityId, sDate, eDate);
        System.out.println(test + "the facility is in use");
    }
    public static void printAssignFacilityToUse(LeaseInfoImpl leaseInfo)
    {
        boolean test = facilityUsageService.assignFacilityToUse(leaseInfo);
        System.out.println("The Facility is useable" + test);
    }
    public static void printVacateFacility(int facilityId, Date vDate)
    {
        boolean test=facilityUsageService.vacateFacility(facilityId, vDate);
        System.out.println(test + "Facility vacated");
    }
    public static void printListInspections()
    {
        List<InspectionImpl> inspectionList = facilityUsageService.listInspections();
        for(InspectionImpl inspection : inspectionList)
        {
            System.out.println(inspection.toString());
        }
    }
    public static void printListActualUsage()
    {
        HashMap<Integer,Integer> actualUsageList = facilityUsageService.listActualUsage();
        Collection<Integer> keys = actualUsageList.values();
        for(Integer key : keys)
        {
            int time = actualUsageList.get(key);
            System.out.println("Facility: " + key + " has been in the industry for " + time+" hours");
        }
    }
    public static void printCalcUsageRate()
    {
          HashMap<Integer,BigDecimal> calcUsageRateList = facilityUsageService.calcUsageRate();
        Collection<BigDecimal> keys = calcUsageRateList.values();
        for(BigDecimal key : keys)
        {
            BigDecimal due = calcUsageRateList.get(key);
            System.out.println("Facility: " + key + " has a due payment of  " + due + "money");
        }
    }
    public static void main(String[] args)
    {
        try
        {
            factory = new Configuration().configure().buildSessionFactory();
        }
        catch(Throwable ex)
        {
            throw new ExceptionInInitializerErroe(ex);
        }
	ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"services.xml","daos.xml","models.xml"});
           // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code."> 
        //create FacilityAddress
        FacilityAddressImpl facilityAddress1 = new FacilityAddressImpl(9,"1830 93rd Street","Chicago","Illinois","60606","BUILDING",null);
        FacilityAddressImpl facilityAddress2 = new FacilityAddressImpl(10,"Montrose","Chicago","Illinois","60665","UNIT",null);
        //end of create FacilityAddress
        
        //create Facility and assign FacilityAddress
        List<FacilityImpl> facilities = new ArrayList<FacilityImpl>();
        FacilityImpl facility1 = new FacilityImpl(9,facilityAddress1,"Cosgrove Inc",100,BigDecimal.valueOf(20000),"BUILDING");
        FacilityImpl facility2 = new FacilityImpl(10,facilityAddress2,"Aquatics Park",200,BigDecimal.valueOf(20000),"UNIT");
        
        facilities.add(facility1);
        facilities.add(facility2);
        facilityAddress1.setFacilities(facilities);
        facilityAddress2.setFacilities(facilities);
        //end of create Facility
        
        //create Building and set Facility
        BuildingImpl building1 = new BuildingImpl(9,facility1,12,80,3);
        BuildingImpl building2 = new BuildingImpl(10,facility2,12,80,3);
        //end of create Building
        
        //create BuildingUnit and assign Facility
        BuildingUnitImpl buildingUnit1 = new BuildingUnitImpl(9,facility1,5,40);
        BuildingUnitImpl buildingUnit2 = new BuildingUnitImpl(10,facility2,5,40);
        //end of create BuildingUnit
        
        //create Inspection and set Fcaility 
        InspectionImpl inspection1 = new InspectionImpl(9,facility1,new Date(),"Ok.");
        InspectionImpl inspection2 = new InspectionImpl(10,facility2,new Date(),"Fail");
        //end of create Inspection 
        
        //create Tenant and set Phone
        TenantImpl tenant1 = new TenantImpl(9,"Bob","Smith","123456789","bsmith@luc.edu","8675309312","322 W. Armitage Chicago");
        TenantImpl tenant2 = new TenantImpl(10,"Sally","Longhart","888888889","slonghart@luc.edu","132435465","123 North W. Street");
        //end of create Tenant 
        
        //create LeaseInfo and set Fcaility and Tenant 
        
        Date end = new Date();
        double securityDeposit = 50;
        LeaseInfoImpl leaseInfo1 = new LeaseInfoImpl(9,facility1,tenant1,new Date(),end, BigDecimal.valueOf(securityDeposit),new Date(),"Clean");
        LeaseInfoImpl leaseInfo2 = new LeaseInfoImpl(10,facility2,tenant2,new Date(),end,BigDecimal.valueOf(securityDeposit),end,"Dirty");
        //end of create LeaseInfo 
        
        //create LeasePayment and set LeaseInfo
        LeasePaymentImpl leasePayment1 = new LeasePaymentImpl(9,leaseInfo1,BigDecimal.TEN,new Date(), BigDecimal.ONE,new Date());
        LeasePaymentImpl leasePayment2 = new LeasePaymentImpl(10,leaseInfo2,BigDecimal.TEN,new Date(), BigDecimal.ONE,new Date());
        //end of create LeasePayment

        //create MaintenanceRequest
        MaintenanceRequestImpl maintenanceRequest1 = new MaintenanceRequestImpl(9,facility1,new Date(),"Luke","Broken lamp,","lhoffman1@northwestern","Shower");
        MaintenanceRequestImpl maintenanceRequest2 = new MaintenanceRequestImpl(10,facility2, new Date(), "Dante","Lights too bright","dante@alligieri.com","Lighting complaint");
        //end of create MaintenanceRequest

        //create MaintenancePhone
        MaintenanceWorkerPhoneImpl albertsPhone = new MaintenanceWorkerPhoneImpl(9,"TMOBILE","FLIP","123456789");
        MaintenanceWorkerPhoneImpl alberts2ndPhone = new MaintenanceWorkerPhoneImpl(10,"SPRINT","BLACKBERRY","99999999");
        //end of create Maintenance Phone
        
        
        //create Maintenance Cost --note--have to set maintenance in maintenance cost later because of unidirectionality of settings for consistency
        MaintenanceImpl maintenance1= new MaintenanceImpl();
        MaintenanceImpl maintenance2 = new MaintenanceImpl();
        MaintenanceCostImpl maintenanceCost1 = new MaintenanceCostImpl(9,maintenance1,99,BigDecimal.ZERO,BigDecimal.TEN,"PaidJ");
        MaintenanceCostImpl maintenanceCost2 = new MaintenanceCostImpl(10,maintenance2,99,BigDecimal.ZERO,BigDecimal.TEN,"isn't paid");
        //end of create Maintenance Cost
        //create Maintenance and set MaintenanceCost and MaintenanceRequest
        maintenance1 = new MaintenanceImpl(9,maintenanceRequest1,"Albert",albertsPhone,new Date(), new Date(), maintenanceCost1);
        maintenance2 = new MaintenanceImpl(10,maintenanceRequest2,"Johnny",alberts2ndPhone,new Date(), new Date(), maintenanceCost2);
        //end of create Maintenance 
        maintenanceCost1.setMaintenanceImpl(maintenance1);
        maintenanceCost2.setMaintenanceImpl(maintenance2);
        
        //create Owner amd posssibly set phone 
        OwnerImpl owner1 = new OwnerImpl(9,"Danny","Martin","123456789","dmartin3@luc.edu","3122248092","6603 Barnes and Nobles");
        OwnerImpl owner2 = new OwnerImpl(10,"Cherry","Martin","151423413","cmartin1@luc.edu","2246280692","1830 W 93rd St");
        //end of create Owner
        
        //create Purchase Info and set Facility and Owner 
        PurchaseInfoImpl purchaseInfo1 = new PurchaseInfoImpl(9,facility1,owner1,new Date(), BigDecimal.ZERO);
        PurchaseInfoImpl purchaseInfo2 = new PurchaseInfoImpl(10,facility2,owner2,new Date(), BigDecimal.TEN);
        //end of create PurchaseInfo
        
        //</editor-fold>
        //insert Maintenance Objects  and print
        printFacilityMaintenanceRequest(maintenanceRequest1);
        printFacilityMaintenanceRequest(maintenanceRequest2);
        printScheduleMaintenance(maintenance1);
        printScheduleMaintenance(maintenance2);
        printListMaintRequests();
        
        printMaintenanceCostForFacility(maintenanceCost1);
        printFacilityProblems(9);
        printDownTimeForFacility(9);
        
        //insert Facility objects and print
        printAddFacility(facility1);
        printAddFacility(facility2);
        printAddFacilityDetail(building1);
        printAddFacilityDetail(building2);
        printFacilityInformation(9);
        printAvailableCapacity(9);
        printListFacilities();
        
        printRemoveFacility(10);
        
        //insert UsageObjects and print
        printAssignFacilityToUse(leaseInfo1);
        printAssignFacilityToUse(leaseInfo2);
        printCalcUsageRate();
        printCalcUsageRate();
        printListInspections();
        printListActualUsage();
        printIsInUseDuringInterval(9,new Date(),new Date());
        printVacateFacility(9,new Date());
        
    }
    
    private static ApplicationContext context = new ClassPathXmlApplicationContext("facility.management.dal.services.xml");
    private static FacilityMaintenanceServiceImpl facilityMaintenanceService = (FacilityMaintenanceServiceImpl) context.getBeans("facilityMaintenanceServiceImpl");//new FacilityMaintenanceServiceImpl();
    private static FacilityServiceImpl facilityService = (FacilityServiceImpl) context.getBeans("facilityServiceImpl");//new FacilityServiceImpl();
    private static FacilityUsageServiceImpl facilityUsageService = (FacilityUsageServiceImpl) context.getBeans("facilityUsageServiceImpl");//new FacilityUsageServiceImpl();
    private static SessionFactory factory;
}
