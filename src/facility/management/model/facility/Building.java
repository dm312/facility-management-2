/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

/**
 *
 * @author dmartin3
 */
public interface Building extends FacilityCharacter {
    Integer getBuildingId();
    void setBuildingId(Integer buildingId);
    FacilityImpl getFacilityImpl();
    void setFacilityImpl(FacilityImpl facilityImpl);
    Integer getNumOfElevators();
    void setNumOfElevators(Integer numOfElevators);
    Integer getNumOfFloors();
    void setNumOfFloors(Integer numOfFloors);
    Integer getNumOfEntrances();
    void setNumOfEntrances(Integer numOfEntrances);

}
