/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

/**
 *
 * @author dmartin3
 */
public interface BuildingUnit {
    
	Integer getBuildingUnitId();
	void setBuildingUnitId(Integer buildingUnitId);
	FacilityImpl getFacilityImpl();
	void setFacilityImpl(FacilityImpl facilityImpl);
	Integer getLevelNum();
	void setLevelNum(Integer levelNum);
	Integer getNumOfRooms();
	void setNumOfRooms(Integer numOfRooms);
}
