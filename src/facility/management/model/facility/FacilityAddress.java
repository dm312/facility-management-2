/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

/**
 *
 * @author dmartin3
 */
public interface FacilityAddress {
    Integer getFacilityAddressId();
    void setFacilityAddressId(Integer facilityAddressId);
    String getStreetAddress();
    void setStreetAddress(String streetAddress);
    String getCity();
    void setCity(String city);
    String getState();
    void setState(String state);
    String getZipCode();
    void setZipCode(String zipCode);
    String getFacilityType();
    void setFacilityType(String facilityType);
}
