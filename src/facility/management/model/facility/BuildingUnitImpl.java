/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

/**
 *
 * @author Danny
 */
public class BuildingUnitImpl implements BuildingUnit {
    private Integer buildingUnitId;
    private FacilityImpl facilityImpl;
    private Integer levelNum;
    private Integer numOfRooms;

    public BuildingUnitImpl(FacilityImpl facilityImpl, Integer levelNum, Integer numOfRooms) {
        this.facilityImpl = facilityImpl;
        this.levelNum = levelNum;
        this.numOfRooms = numOfRooms;
    }
    
    public void toString2()
    {
        System.out.println("Building Unit Id: " + this.buildingUnitId+ " Level Number: " + levelNum + " Number of Rooms: " + numOfRooms);
    }

    public BuildingUnitImpl(Integer buildingUnitId, FacilityImpl facilityImpl, Integer levelNum, Integer numOfRooms) {
        this.buildingUnitId = buildingUnitId;
        this.facilityImpl = facilityImpl;
        this.levelNum = levelNum;
        this.numOfRooms = numOfRooms;
    }
    	public Integer getBuildingUnitId()
        {
            return this.buildingUnitId;
        }
	public void setBuildingUnitId(Integer buildingUnitId)
        {
            this.buildingUnitId = buildingUnitId;
        }
	public FacilityImpl getFacilityImpl()
        {
            return this.facilityImpl;
        }
	public void setFacilityImpl(FacilityImpl facilityImpl)
        {
            this.facilityImpl= facilityImpl;
        }
	public Integer getLevelNum()
        {
            return this.levelNum;
        }
	public void setLevelNum(Integer levelNum)
        {
            this.levelNum = levelNum;
        }
	public Integer getNumOfRooms()
        {
            return this.numOfRooms;
        }
	public void setNumOfRooms(Integer numOfRooms)
        {
            this.numOfRooms= numOfRooms;
        }
}
