/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;
import java.math.BigDecimal;
/**
 *
 * @author dmartin3
 */
public interface Facility {
    
    Integer getFacilityId();
    void setFacilityId(Integer facilityId);
    FacilityAddressImpl getFacilityAddressImpl();
    void setFacilityAddressImpl(FacilityAddressImpl facilityAddressImpl);
    String getFacilityName();
    void setFacilityName(String facilityName);
    Integer getCapacity();
    void setCapacity(Integer capacity);
    BigDecimal getTotalArea();
    void setTotalArea(BigDecimal totalArea);
    String getFacilityType();
    void setFacilityType(String facilityType);
    
}
