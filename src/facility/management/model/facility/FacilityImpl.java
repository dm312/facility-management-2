/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

import java.math.BigDecimal;

/**
 *
 * @author Danny
 */
public class FacilityImpl implements Facility {
    public FacilityImpl()
    {}
    public FacilityImpl(Integer facilityId, FacilityAddressImpl facilityAddressImpl, String facilityName, Integer capacity, BigDecimal totalArea, String facilityType) {
        this.facilityId = facilityId;
        this.facilityAddressImpl = facilityAddressImpl;
        this.facilityName = facilityName;
        this.capacity = capacity;
        this.totalArea = totalArea;
        this.facilityType = facilityType;
    }

    @Override
    public Integer getFacilityId() {
        return this.facilityId;
    }

    @Override
    public void setFacilityId(Integer facilityId) {
        this.facilityId= facilityId;
    }

    @Override
    public FacilityAddressImpl getFacilityAddressImpl() {
        return this.facilityAddressImpl;
    }

    @Override
    public void setFacilityAddressImpl(FacilityAddressImpl facilityAddressImpl) {
        this.facilityAddressImpl= facilityAddressImpl; 
    }

    @Override
    public String getFacilityName() {
        return this.facilityName;
    }

    @Override
    public void setFacilityName(String facilityName) {
        this.facilityName= facilityName;
    }

    @Override
    public Integer getCapacity() {
        return this.capacity;
    }

    @Override
    public void setCapacity(Integer capacity) {
        this.capacity= capacity;
    }

    @Override
    public BigDecimal getTotalArea() {
        return this.totalArea;
    }

    @Override
    public void setTotalArea(BigDecimal totalArea) {
        this.totalArea= totalArea;
    }

    @Override
    public String getFacilityType() {
        return this.facilityType;
    }

    @Override
    public void setFacilityType(String facilityType) {
        this.facilityType= facilityType;
    }
    
    public void toString2()
    {
        System.out.println("Facility Id: " + facilityId + " Facility Name: " + facilityName + " Capacity: " + capacity + " totalArea: " + totalArea + " Facility Type: "  + facilityType);
    }

    public FacilityImpl(FacilityAddressImpl facilityAddressImpl, String facilityName, Integer capacity, BigDecimal totalArea, String facilityType) {
        this.facilityAddressImpl = facilityAddressImpl;
        this.facilityName = facilityName;
        this.capacity = capacity;
        this.totalArea = totalArea;
        this.facilityType = facilityType;
    }
    
    private Integer facilityId;
    private FacilityAddressImpl facilityAddressImpl;
    private String facilityName;
    private Integer capacity;
    private BigDecimal totalArea;
    private String facilityType;    
}
