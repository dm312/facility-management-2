/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

import java.util.List;

/**
 *
 * @author Danny
 */
public class FacilityAddressImpl implements FacilityAddress{
    public FacilityAddressImpl()
    {}
    public FacilityAddressImpl(Integer facilityAddressId, String streetAddress, String city, String state, String zipcode, String facilityType, List<FacilityImpl> facilities) {
        this.facilityAddressId = facilityAddressId;
        this.streetAddress = streetAddress;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.facilityType = facilityType;
        this.facilities = facilities;
    }

    @Override
    public Integer getFacilityAddressId() {
        return this.facilityAddressId;
    }

    @Override
    public void setFacilityAddressId(Integer facilityAddressId) {
        this.facilityAddressId= facilityAddressId;
    }

    @Override
    public String getStreetAddress() {
        return this.streetAddress;
    }

    @Override
    public void setStreetAddress(String streetAddress) {
        this.streetAddress= streetAddress;
    }

    @Override
    public String getCity() {
        return this.city;
    }

    @Override
    public void setCity(String city) {
        this.city= city;
    }

    @Override
    public String getState() {
        return this.state;
    }

    @Override
    public void setState(String state) {
        this.state=state;
    }

    @Override
    public String getZipCode() {
        return this.zipcode;
    }

    @Override
    public void setZipCode(String zipCode) {
        this.zipcode=zipcode;
    }

    @Override
    public String getFacilityType() {
        return this.facilityType;
    }

    @Override
    public void setFacilityType(String facilityType) {
        this.facilityType= facilityType;
    }
    public List<FacilityImpl> getFacilities()
    {
        return this.facilities;
    }
    public void setFacilities(List<FacilityImpl> facilities)
    {
        this.facilities=facilities;
    }
    
    public void toString2()
    {
        System.out.println("Facility Address Id: " + facilityAddressId + " Street Address: " + streetAddress + " City: " + city + " State: " + state+ " Zipcode: " + zipcode + " Facility Type: " + facilityType);
    }
    
    private Integer facilityAddressId;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String facilityType;
    private List<FacilityImpl> facilities;
    
    
}
