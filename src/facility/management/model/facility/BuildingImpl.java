/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.facility;

/**
 *
 * @author Danny
 */
public class BuildingImpl implements Building{

    
    public Integer getBuildingId()
    {
        return this.buildingId;
    }
    public BuildingImpl()
    {}
    public BuildingImpl(Integer buildingId, FacilityImpl facilityImpl, Integer numOfElevators, Integer numOfFloors, Integer numOfEntrances) {
        this.buildingId = buildingId;
        this.facilityImpl = facilityImpl;
        this.numOfElevators = numOfElevators;
        this.numOfFloors = numOfFloors;
        this.numOfEntrances = numOfEntrances;
    }

    public BuildingImpl(FacilityImpl facilityImpl, Integer numOfElevators, Integer numOfFloors, Integer numOfEntrances) {
        this.facilityImpl = facilityImpl;
        this.numOfElevators = numOfElevators;
        this.numOfFloors = numOfFloors;
        this.numOfEntrances = numOfEntrances;
    }
    public void setBuildingId(Integer buildingId)
    {
        this.buildingId = buildingId;
    }
    public FacilityImpl getFacilityImpl()
    {
        return this.facilityImpl;
    }
    public void setFacilityImpl(FacilityImpl facilityImpl)
    {
        this.facilityImpl = facilityImpl;
    }
    public Integer getNumOfElevators()
    {
        return this.numOfElevators;
    }
    public void setNumOfElevators(Integer numOfElevators)
    {
        this.numOfElevators = numOfElevators;
    }
    public Integer getNumOfFloors()
    {
        return this.numOfFloors;
    }
    public void setNumOfFloors(Integer numOfFloors)
    {
        this.numOfFloors = numOfFloors;
    }
    public Integer getNumOfEntrances()
    {
        return this.numOfEntrances;
    }
    public void setNumOfEntrances(Integer numOfEntrances)
    {
        this.numOfEntrances = numOfEntrances;
    }
    public void toString2()
    {
        System.out.println("Building Id: " + this.buildingId + "Number of Elevators: " + numOfElevators + "Number of Floors: " + numOfFloors + "Number of entrances: " + numOfEntrances);
    }
    public static void main(String[] args)
    {
        
    }
    
    private Integer buildingId;
    private FacilityImpl facilityImpl;
    private Integer numOfElevators;
    private Integer numOfFloors;
    private Integer numOfEntrances;
    
}
