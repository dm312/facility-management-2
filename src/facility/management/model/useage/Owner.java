/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

/**
 *
 * @author Danny
 */
public interface Owner {
    
    Integer getOwnerId();
    void setOwnerId(Integer ownerId);
    String getFirstName();
    void setFirstName(String firstName);
    String getLastName();
    void setLastName(String lastName);
    String getSSN();
    void setSSN(String ssn);
    String getEmail();
    void setEmail(String email);
    String getPhone();
    void setPhone(String phone);
    String getAddress();
    void setAddress(String address);
    
}
