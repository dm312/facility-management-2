/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

/**
 *
 * @author Danny
 */
public class TenantPhoneImpl implements TenantPhone{

    @Override
    public Integer getPhoneId() {
        return this.tenantPhoneid;
    }

    @Override
    public void setPhoneId(Integer facilityPhoneId) {
        this.tenantPhoneid = facilityPhoneId;
    }

    @Override
    public String getPhoneCarrier() {
        return this.tenantCarrier;
    }

    @Override
    public void setPhoneCarrier(String facilityPhoneCarrier) {
        this.tenantCarrier = facilityPhoneCarrier;
    }

    @Override
    public String getPhoneType() {
        return this.tenantType;
    }

    @Override
    public void setPhoneType(String facilityPhoneType) {
        this.tenantType = facilityPhoneType;
    }

    @Override
    public String getPhoneNumber() {
        return this.tenantNumber;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.tenantNumber = phoneNumber;
    }
    
    public static void main(String[] args)
    {}
    
    private int tenantPhoneid;
    private String tenantCarrier;
    private String tenantType;
    private String tenantNumber;
}
