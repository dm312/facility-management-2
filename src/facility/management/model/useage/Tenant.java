/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

/**
 *
 * @author Danny
 */
public interface Tenant{
    
    Integer getTenantId();
    void setTenantId(Integer tenantId);
    String getFirstName();
    void setFirstName(String firstNam);
    String getLastName();
    void setLastName(String lastNam);
    String getssn();
    void setssn(String ssn);
    String getEmail();
    void setEmail(String email);
    String getPhone();
    void setPhone(String phon);
    String getAddress();
    void setAddress(String address);
    
}
