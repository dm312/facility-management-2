/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

/**
 *
 * @author Danny
 */
public class OwnerImpl implements Owner {

    public OwnerImpl(Integer ownerId, String firstName, String lastName, String SSN, String email, String phone, String address) {
        this.ownerId = ownerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.SSN = SSN;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    @Override
    public Integer getOwnerId() {
     return this.ownerId;   
    }

    @Override
    public void setOwnerId(Integer ownerId) {
        this.ownerId= ownerId;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName= firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName= lastName;
    }

    @Override
    public String getSSN() {
        return this.SSN;
        
    }

    @Override
    public void setSSN(String ssn) {
        SSN= ssn;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email= email;
    }

    @Override
    public String getPhone() {
        return this.phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone= phone;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public void setAddress(String address) {
        this.address= address;
    }
    
    public static void main(String[] args)
    {
        
    }
    
    private Integer ownerId;
    private String firstName;
    private String lastName;
    private String SSN;
    private String email;
    private String phone;
    private String address;
    
    
}
