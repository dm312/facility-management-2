/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class LeasePaymentImpl implements LeasePayment {

    public LeasePaymentImpl(Integer leasePaymentId, LeaseInfoImpl leaseInfoImpl, BigDecimal amountDue, Date paymentDate, BigDecimal amountPaid, Date dueDate) {
        this.leasePaymentId = leasePaymentId;
        this.leaseInfoImpl = leaseInfoImpl;
        this.amountDue = amountDue;
        this.paymentDate = paymentDate;
        this.amountPaid = amountPaid;
        this.dueDate = dueDate;
    }

   
    @Override
    public Integer getLeasePaymentId() {
        return this.leasePaymentId;
    }

    @Override
    public void setLeasePaymentId(Integer leasePaymentId) {
        this.leasePaymentId= leasePaymentId;
    }

    @Override
    public LeaseInfoImpl getLeaseInfoImpl() {
        return this.leaseInfoImpl;
    }

    @Override
    public void setLeaseInfoImpl(LeaseInfoImpl leaseInfoImpl) {
        this.leaseInfoImpl= leaseInfoImpl;
    }

    @Override
    public BigDecimal getAmountDue() {
        return this.amountDue;
    }

    @Override
    public void setAmountDue(BigDecimal amountDue) {
        this.amountDue= amountDue;
    }

    @Override
    public BigDecimal getAmountPaid() {
        return this.amountPaid;
    }

    @Override
    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid= amountPaid;
    }

    @Override
    public Date getDueDate() {
        return this.dueDate;
    }

    @Override
    public void setDueDate(Date dueDate) {
        this.dueDate= dueDate;
    }
    
    private Integer leasePaymentId;
    private LeaseInfoImpl leaseInfoImpl;
    private BigDecimal amountDue;
    private Date paymentDate;
    private BigDecimal amountPaid;
    private Date dueDate;
    
}
