/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

/**
 *
 * @author Danny
 */
public class TenantImpl implements Tenant{

    public TenantImpl(Integer tenantId, String firstName, String lastName, String ssn, String email, String phone, String address) {
        this.tenantId = tenantId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.ssn = ssn;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    @Override
    public Integer getTenantId() {
        return this.tenantId;
    }

    @Override
    public void setTenantId(Integer tenantId) {
        this.tenantId= tenantId;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(String firstNam) {
        firstName= firstNam;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public void setLastName(String lastNam) {
        this.lastName= lastNam;
    }

    @Override
    public String getssn() {
        return this.ssn;
    }

    @Override
    public void setssn(String ssn) {
        this.ssn= ssn;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email=email;
    }

    @Override
    public String getPhone() {
        return this.phone;
    }

    @Override
    public void setPhone(String phon) {
        phone=phon;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public void setAddress(String address) {
        this.address= address;
    }
    
    public static void main(String[] args)
    {
        
    }
    
    private Integer tenantId;
    private String firstName;
    private String lastName;
    private String ssn;
    private String email;
    private String phone;
    private String address;
}
