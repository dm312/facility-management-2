/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;
import facility.management.model.facility.FacilityImpl;
import java.util.Date;

/**
 *
 * @author dmartin3
 */
public interface Inspection {
    
    Integer getInspectionId();
    void setInspectionId(Integer inspectionId);
    FacilityImpl getFacilityImpl();
    void setFacilityImpl(FacilityImpl facilityImpl);
    Date getInspectionDate();
    void setInspectionDate(Date inspectionDate);
    String getInspectionResult();
    void setInspectionResult(String inspectionresult);
    
}
