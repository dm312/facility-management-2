/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author dmartin3
 */
public interface LeasePayment {
    
    Integer getLeasePaymentId();
    void setLeasePaymentId(Integer leasePaymentId);
    LeaseInfoImpl getLeaseInfoImpl();
    void setLeaseInfoImpl(LeaseInfoImpl leaseInfoImpl);
    BigDecimal getAmountDue();
    void setAmountDue(BigDecimal amountDue);
    BigDecimal getAmountPaid();
    void setAmountPaid(BigDecimal amountPaid);
    Date getDueDate();
    void setDueDate(Date dueDate);
    
    
}
