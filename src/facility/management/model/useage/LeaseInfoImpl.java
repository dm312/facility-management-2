/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.facility.FacilityImpl;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class LeaseInfoImpl implements LeaseInfo {

    public LeaseInfoImpl(Integer leaseInfoId, FacilityImpl facilityImpl, TenantImpl tenantImpl, Date startDate, Date endDate, BigDecimal securityDeposit, Date terminationDate, String status) {
        this.leaseInfoId = leaseInfoId;
        this.facilityImpl = facilityImpl;
        this.tenantImpl = tenantImpl;
        this.startDate = startDate;
        this.endDate = endDate;
        this.securityDeposit = securityDeposit;
        this.terminationDate = terminationDate;
        this.status = status;
    }

    public LeaseInfoImpl(FacilityImpl facilityImpl, TenantImpl tenantImpl, Date startDate, Date endDate, BigDecimal securityDeposit, Date terminationDate, String status) {
        this.facilityImpl = facilityImpl;
        this.tenantImpl = tenantImpl;
        this.startDate = startDate;
        this.endDate = endDate;
        this.securityDeposit = securityDeposit;
        this.terminationDate = terminationDate;
        this.status = status;
    }

    
    @Override
    public Integer getLeaseInfoId(){
        return this.leaseInfoId;
    }
    @Override
    public void setLeaseInfoId(Integer leaseInfoId) {
        this.leaseInfoId= leaseInfoId;
    }

    @Override
    public FacilityImpl getFacilityImpl() {
        return this.facilityImpl;
    }

    @Override
    public void setFacilityImpl(FacilityImpl facilityImpl) {
        this.facilityImpl= facilityImpl;
    }

    @Override
    public TenantImpl getTenantImpl() {
        return this.tenantImpl;
    }

    @Override
    public void setTenantImpl(TenantImpl tenantImpl) {
        this.tenantImpl= tenantImpl;
    }

    @Override
    public Date getStartDate() {
        return this.startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate= startDate;
    }

    @Override
    public Date getEndDate() {
        return this.endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        this.endDate= endDate;
    }

    @Override
    public BigDecimal getSecurityDeposit() {
        return this.securityDeposit;
    }

    @Override
    public void setSecurityDeposit(BigDecimal securityDeposit) {
        this.securityDeposit= securityDeposit;
    }

    @Override
    public Date getTerminationDate() {
        return this.terminationDate;
    }

    @Override
    public void setTerminationDate(Date terminationDate) {
        this.terminationDate= terminationDate;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status= status;
    }
    
    public static void main(String[] args)
    {
        
    }
    private Integer leaseInfoId;
    private FacilityImpl facilityImpl;
    private TenantImpl tenantImpl;
    private Date startDate;
    private Date endDate;
    private BigDecimal securityDeposit;
    private Date terminationDate;
    private String status;   
}
