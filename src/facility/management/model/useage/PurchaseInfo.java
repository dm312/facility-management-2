/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.facility.FacilityImpl;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author dmartin3
 */
public interface PurchaseInfo {
    
    Integer getPurchaseInfoId();
    void setPurchaseInfoId(Integer purchaseId);
    FacilityImpl getFacilityImpl();
    void setFacilityImpl(FacilityImpl facilityImpl);
    OwnerImpl getOwnerImpl();
    void setOwnerImpl(OwnerImpl ownerImpl);
    Date getPurchaseDate();
    void setPurchaseDate(Date purchaseDate);
    BigDecimal getPurchasePrice();
    void setPurchasePrice(BigDecimal purchasePrice);
    
    
}
