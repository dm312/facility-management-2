/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.facility.FacilityImpl;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class InspectionImpl implements Inspection {

    public InspectionImpl(Integer inspectionId, FacilityImpl facilityImpl, Date inspectionDate, String inspectionResult) {
        this.inspectionId = inspectionId;
        this.facilityImpl = facilityImpl;
        this.inspectionDate = inspectionDate;
        this.inspectionResult = inspectionResult;
    }

   
    
    @Override
    public Integer getInspectionId() {
        return this.inspectionId;
    }

    @Override
    public void setInspectionId(Integer inspectionId) {
        this.inspectionId= inspectionId;
    }

    @Override
    public FacilityImpl getFacilityImpl() {
        return this.facilityImpl;
    }

    @Override
    public void setFacilityImpl(FacilityImpl facilityImpl) {
        this.facilityImpl= facilityImpl;
    }

    @Override
    public Date getInspectionDate() {
        return this.inspectionDate;
    }

    @Override
    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate= inspectionDate;
    }

    @Override
    public String getInspectionResult() {
        return this.inspectionResult;
    }

    @Override
    public void setInspectionResult(String inspectionresult) {
        this.inspectionResult= inspectionresult;
    }

    @Override
    public String toString() {
        return "InspectionImpl{" + "inspectionId=" + inspectionId + ", facilityImpl=" + facilityImpl + ", inspectionDate=" + inspectionDate + ", inspectionResult=" + inspectionResult + '}';
    }
    
    public static void main(String[] args)
    {
        
    }
    private Integer inspectionId;
    private FacilityImpl facilityImpl;
    private Date inspectionDate;
    private String inspectionResult;
}
