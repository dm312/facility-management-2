/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.facility.FacilityImpl;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author dmartin3
 */
public interface LeaseInfo {
    
    Integer getLeaseInfoId();
    void setLeaseInfoId(Integer leaseInfoId);
    FacilityImpl getFacilityImpl();
    void setFacilityImpl(FacilityImpl facilityImpl);
    TenantImpl getTenantImpl();
    void setTenantImpl(TenantImpl tenantImpl);
    Date getStartDate();
    void setStartDate(Date startDate);
    Date getEndDate();
    void setEndDate(Date endDate);
    BigDecimal getSecurityDeposit();
    void setSecurityDeposit(BigDecimal securityDeposit);
    Date getTerminationDate();
    void setTerminationDate(Date terminationDate);
    String getStatus();
    void setStatus(String status);
    
    
}
