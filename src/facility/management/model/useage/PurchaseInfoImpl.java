/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.facility.FacilityImpl;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class PurchaseInfoImpl implements PurchaseInfo {

    public PurchaseInfoImpl(Integer purchaseInfoId, FacilityImpl facilityImpl, OwnerImpl ownerImpl, Date purchaseDate, BigDecimal purchasePrice) {
        this.purchaseInfoId = purchaseInfoId;
        this.facilityImpl = facilityImpl;
        this.ownerImpl = ownerImpl;
        this.purchaseDate = purchaseDate;
        this.purchasePrice = purchasePrice;
    }

    @Override
    public Integer getPurchaseInfoId() {
        return this.purchaseInfoId;
    }

    @Override
    public void setPurchaseInfoId(Integer purchaseId) {
        this.purchaseInfoId= purchaseId;
    }

    @Override
    public FacilityImpl getFacilityImpl() {
        return this.facilityImpl;
    }

    @Override
    public void setFacilityImpl(FacilityImpl facilityImpl) {
        this.facilityImpl= facilityImpl;
    }

    @Override
    public OwnerImpl getOwnerImpl() {
        return this.ownerImpl;
    }

    @Override
    public void setOwnerImpl(OwnerImpl ownerImpl) {
        this.ownerImpl= ownerImpl;
    }

    @Override
    public Date getPurchaseDate() {
        return this.purchaseDate;
    }

    @Override
    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate= purchaseDate;
    }

    @Override
    public BigDecimal getPurchasePrice() {
        return this.purchasePrice;
    }

    @Override
    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice= purchasePrice;
    }
    
    public static void main(String[] args)
    {
        
    }
    private Integer purchaseInfoId;
    private FacilityImpl facilityImpl;
    private OwnerImpl ownerImpl;
    private Date purchaseDate;
    private BigDecimal purchasePrice;
    
}
