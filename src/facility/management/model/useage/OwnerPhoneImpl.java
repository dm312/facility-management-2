/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.useage;

import facility.management.model.maintenance.Phone;

/**
 *
 * @author Danny
 */
public class OwnerPhoneImpl implements Phone {

    @Override
    public Integer getPhoneId() {
        return this.ownerPhoneId;
    }

    @Override
    public void setPhoneId(Integer facilityPhoneId) {
        this.ownerPhoneId=facilityPhoneId;
    }

    
    public String getPhoneCarrier() {
        return this.carrier;
    }

    public void setPhoneCarrier(String facilityPhoneCarrier) {
        carrier=facilityPhoneCarrier;
    }

    public String getPhoneType() {
        return this.type;
    }

    public void setPhoneType(String facilityPhoneType) {
        this.type=facilityPhoneType;
    }

    @Override
    public String getPhoneNumber() {
        return number;
    }
 
    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.number = phoneNumber;
    }
  
    public static void main(String[] args)
    {}
    
    private int ownerPhoneId;
    private String carrier;
    private String type;
    private String number;
}
