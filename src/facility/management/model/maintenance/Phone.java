/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

/**
 *
 * @author Danny
 */
public interface Phone {
    
    Integer getPhoneId();
    void setPhoneId(Integer facilityPhoneId);
    String getPhoneNumber();
    void setPhoneNumber(String phoneNumber);
}
