/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

import java.math.BigDecimal;

/**
 *
 * @author dmartin3
 */
public interface MaintenanceCost {
 Integer getMaintenanceCostId();
 void setMaintenanceCostId(Integer maintenanceCostId);
 MaintenanceImpl getMaintenanceImpl();
 void setMaintenanceImpl(MaintenanceImpl maintenanceImpl);
 Integer getMaterialCost();
 void setMaterialCost(Integer materialCost);
 BigDecimal getLaborCost();
 void setLaborCost(BigDecimal laborCost);
 BigDecimal getTotalCost();
 void setTotalCost(BigDecimal totalCost);
 String getIsPaid();
 void setIsPaid(String isPaid);
 
}
