/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

import facility.management.model.facility.FacilityImpl;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class MaintenanceRequestImpl implements MaintenanceRequest{
    public MaintenanceRequestImpl()
    {}

    public MaintenanceRequestImpl(FacilityImpl facilityImpl, Date requestDate, String nameOfRequester, String detail, String email, String subject) {
        this.facilityImpl = facilityImpl;
        this.requestDate = requestDate;
        this.nameOfRequester = nameOfRequester;
        this.detail = detail;
        this.email = email;
        this.subject = subject;
    }
    
    public MaintenanceRequestImpl(Integer maintenanceRequestId, FacilityImpl facilityImpl, Date requestDate, String nameOfRequester, String detail, String email, String subject) {
        this.maintenanceRequestId = maintenanceRequestId;
        this.facilityImpl = facilityImpl;
        this.requestDate = requestDate;
        this.nameOfRequester = nameOfRequester;
        this.detail = detail;
        this.email = email;
        this.subject = subject;
    }

    public void toString2() {
        System.out.println("MaintenanceRequestImpl{" + "maintenanceRequestId=" + maintenanceRequestId + ", facilityImpl=" + facilityImpl + ", requestDate=" + requestDate + ", nameOfRequester=" + nameOfRequester + ", detail=" + detail + ", email=" + email + ", subject=" + subject + '}');
    }
    
    @Override
    public Integer getMaintenanceRequestId() {
        return this.maintenanceRequestId;
    }

    @Override
    public void setMaintenanceRequestId(Integer maintenanceRequestId) {
        this.maintenanceRequestId= maintenanceRequestId;
    }

    @Override
    public FacilityImpl getFacilityImpl() {
        return this.facilityImpl;
    }

    @Override
    public void setFacilityImpl(FacilityImpl facilityImpl) {
        this.facilityImpl= facilityImpl;
    }

    @Override
    public Date getRequestDate() {
        return this.requestDate;
    }

    @Override
    public void setRequestDate(Date requestDate) {
        this.requestDate= requestDate;
    }

    @Override
    public String getNameOfRequester() {
        return this.nameOfRequester;
    }

    @Override
    public void setNameOfRequester(String nameOfRequester) {
        this.nameOfRequester= nameOfRequester;
    }

    @Override
    public String getDetail() {
        return this.detail;
    }

    @Override
    public void setDetail(String detail) {
        this.detail= detail;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email= email;
    }

    @Override
    public String getSubject() {
        return this.subject;
    }

    @Override
    public void setSubject(String subject) {
        this.subject= subject;
    }
    
    public static void main(String[] args)
    {
        
    }

    private Integer maintenanceRequestId;
    private FacilityImpl facilityImpl;
    private Date requestDate;
    private String nameOfRequester;
    private String detail;
    private String email;
    private String subject; 
    
}
