/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

import facility.management.util.DateUtil;
import java.util.Date;

/**
 *
 * @author Danny
 */
public class MaintenanceImpl implements Maintenance{

    public MaintenanceImpl()
    {}
    public MaintenanceImpl(Integer maintenanceId, MaintenanceRequestImpl maintenanceRequestImpl, String workerName, MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl, Date startDateTime, Date endDateTime, MaintenanceCostImpl maintenanceCostImpl) {
        this.maintenanceId = maintenanceId;
        this.maintenanceRequestImpl = maintenanceRequestImpl;
        this.workerName = workerName;
        this.maintenanceWorkerPhoneImpl = maintenanceWorkerPhoneImpl;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.maintenanceCostImpl = maintenanceCostImpl;
    }

    public MaintenanceImpl(MaintenanceRequestImpl maintenanceRequestImpl, String workerName, MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl, Date startDateTime, Date endDateTime, MaintenanceCostImpl maintenanceCostImpl) {
        this.maintenanceRequestImpl = maintenanceRequestImpl;
        this.workerName = workerName;
        this.maintenanceWorkerPhoneImpl = maintenanceWorkerPhoneImpl;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.maintenanceCostImpl = maintenanceCostImpl;
    }

    public void toString2() {
        System.out.println( "MaintenanceImpl{" + "maintenanceId=" + maintenanceId + ", maintenanceRequestImpl=" + maintenanceRequestImpl + ", workerName=" + workerName + ", maintenanceWorkerPhoneImpl=" + maintenanceWorkerPhoneImpl + ", startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", maintenanceCostImpl=" + maintenanceCostImpl + '}');
    }


    @Override
    public Integer getMaintenanceId() {
        return maintenanceId;
    }

    @Override
    public void setMaintenanceId(Integer maintenanceId) {
        this.maintenanceId= maintenanceId;
    }

    @Override
    public MaintenanceRequestImpl getMaintenanceRequestImpl() {
        return this.maintenanceRequestImpl;
    }

    @Override
    public void setMaintenanceRequestImpl(MaintenanceRequestImpl maintenanceRequestImpl) {
        this.maintenanceRequestImpl= maintenanceRequestImpl;
    }

    @Override
    public String getWorkerName() {
        return this.workerName;
    }

    @Override
    public void setWorkerName(String workerName) {
        this.workerName= workerName;
    }

    @Override
    public MaintenanceWorkerPhoneImpl getWorkerPhoneImpl() {
        return this.maintenanceWorkerPhoneImpl;
    }

    @Override
    public void setWorkerPhoneImpl(MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl) {
        this.maintenanceWorkerPhoneImpl = maintenanceWorkerPhoneImpl;
    }


    @Override
    public Date getStartDateTime() {
        return this.startDateTime;
    }

    @Override
    public void setStartDateTime(Date startDateTime) {
        this.startDateTime= startDateTime;
    }

    @Override
    public Date getEndDateTime() {
        return this.endDateTime;
    }

    @Override
    public void setEndDateTime(Date endDateTime) {
        this.endDateTime= endDateTime;
    }

    @Override
    public MaintenanceCostImpl getMaintenanceCostImpl() {
        return this.maintenanceCostImpl;
    }

    @Override
    public void setMaintenanceCostImpl(MaintenanceCostImpl maintenanceCostImpl) {
        this.maintenanceCostImpl= maintenanceCostImpl;
    }
    
    
    public static void main(String[] args)
    {
    /* /*   MaintenanceImpl maintenanceImpl = new MaintenanceImpl();
        Date start = DateUtil.createDateTime(9, 9, 9, 9, 9);
        Date end = DateUtil.createDateTime(9, 9, 9, 9, 9);

        maintenanceImpl.setEndDateTime(start); maintenanceImpl.setStartDateTime(end);
        maintenanceImpl.setMaintenanceId(9);
        maintenanceImpl.setWorkerName("Bob");
        maintenanceImpl.setWorkerPhone("9999");
        maintenanceImpl.toString2();
/**/
    
    
    }
    private Integer maintenanceId;
    private MaintenanceRequestImpl maintenanceRequestImpl;
    private String workerName;
    private MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl;
    private Date startDateTime;
    private Date endDateTime;
    private MaintenanceCostImpl maintenanceCostImpl;
    
}
