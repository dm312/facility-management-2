/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;
/**
 *
 * @author Danny
 */
public class MaintenanceWorkerPhoneImpl implements MaintenanceWorkerPhone {
    public MaintenanceWorkerPhoneImpl()
    {}
    public MaintenanceWorkerPhoneImpl(Integer PhoneId, String PhoneCarrier, String PhoneType, String PhoneNumber) {
        this.PhoneId = PhoneId;
        this.PhoneCarrier = PhoneCarrier;
        this.PhoneType = PhoneType;
        this.PhoneNumber = PhoneNumber;
    }

    @Override
    public void setPhoneType(MaintenancePhoneType param) {
        //ANDROID BLACKBERRY IPHONE FLIP
       if(param == MaintenancePhoneType.ANDROID)
           PhoneType = "ANDROID";
       if(param == MaintenancePhoneType.BLACKBERRY)
           PhoneType = "BLACKBERRY";
       if(param == MaintenancePhoneType.FLIP)
           PhoneType = "FLIP";
       else
           PhoneType = "IPHONE";
    }

    @Override
    public String getPhoneType() {
        if(PhoneType == null || PhoneType.equals(""))
            System.out.println("Phone type required");
        
      return this.PhoneType;
    }

    @Override
    public void setPhoneCarrier(MaintenancePhoneCarrier param) {
      
       if(param == MaintenancePhoneCarrier.ATT)
           PhoneCarrier = "ATT";
       if(param == MaintenancePhoneCarrier.PREPAID)
           PhoneCarrier = "PREPAID";
       if(param == MaintenancePhoneCarrier.SPRINT)
           PhoneCarrier = "SPRINT";
       else
           PhoneCarrier = "TMOBILE";
        // TMOBILE, ATT, PREPAID,SPRINT
    }

    @Override
    public String getPhoneCarrier() {
        return this.PhoneCarrier;
    }

  
    @Override
    public Integer getPhoneId()
    {
        return this.PhoneId;
    }
    public void setPhoneId(Integer PhoneId)
    {
        this.PhoneId= PhoneId;
    }
   
    public String getPhoneNumber()
    {
        return this.PhoneNumber;
    }
    public void setPhoneNumber(String PhoneNumber)
    {
        this.PhoneNumber= PhoneNumber;
    }
        
    public void toString2()
    {
        System.out.println("Facility Phone Id: " + PhoneId+ " Facility Phone Carrier: " + PhoneCarrier + " Facility Phone Type: " + PhoneType+ " Facility PHone Number: " + PhoneNumber); 
    }
    
    private Integer PhoneId;
    private String PhoneCarrier;
    private String PhoneType;
    private String PhoneNumber; 
    
}
