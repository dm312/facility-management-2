/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;
import java.util.Date;
/**
 *
 * @author dmartin3
 */
public interface Maintenance {
    Integer getMaintenanceId();
    void setMaintenanceId(Integer maintenanceId);
    MaintenanceRequestImpl getMaintenanceRequestImpl();
    void setMaintenanceRequestImpl(MaintenanceRequestImpl maintenanceRequestImpl);
    String getWorkerName();
    void setWorkerName(String workerName);
    MaintenanceWorkerPhoneImpl getWorkerPhoneImpl();
    void setWorkerPhoneImpl(MaintenanceWorkerPhoneImpl maintenanceWorkerPhoneImpl);
    Date getStartDateTime();
    void setStartDateTime(Date startDateTime);
    Date getEndDateTime();
    void setEndDateTime(Date endDateTime);
    MaintenanceCostImpl getMaintenanceCostImpl();
    void setMaintenanceCostImpl(MaintenanceCostImpl maintenanceCostImpl);
    
}
