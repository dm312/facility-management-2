/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

import facility.management.model.facility.FacilityImpl;
import java.util.Date;

/**
 *
 * @author dmartin3
 */
public interface MaintenanceRequest {
    Integer getMaintenanceRequestId();
    void setMaintenanceRequestId(Integer maintenanceRequestId);
    FacilityImpl getFacilityImpl();
    void setFacilityImpl(FacilityImpl facilityImpl);
    Date getRequestDate();
    void setRequestDate(Date requestDate);
    String getNameOfRequester();
    void setNameOfRequester(String nameOfRequester);
    String getDetail();
    void setDetail(String detail);
    String getEmail();
    void setEmail(String email);
    String getSubject();
    void setSubject(String subject);
    
}
