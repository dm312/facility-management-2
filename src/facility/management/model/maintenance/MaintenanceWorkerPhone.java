/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

/**
 *
 * @author Danny
 */
public interface MaintenanceWorkerPhone extends Phone{

    public void setPhoneType(MaintenancePhoneType param);
    public String getPhoneType();
    public void setPhoneCarrier(MaintenancePhoneCarrier param);
    public String getPhoneCarrier();
    

}
