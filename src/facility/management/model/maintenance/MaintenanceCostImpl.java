/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.model.maintenance;

import java.math.BigDecimal;

/**
 *
 * @author Danny
 */
public class MaintenanceCostImpl implements MaintenanceCost{
    public MaintenanceCostImpl()
    {}
    public MaintenanceCostImpl(Integer maintenanceCostId, MaintenanceImpl maintenanceImpl, Integer materialCost, BigDecimal laborCost, BigDecimal totalCost, String isPaid) {
        this.maintenanceCostId = maintenanceCostId;
        this.maintenanceImpl = maintenanceImpl;
        this.materialCost = materialCost;
        this.laborCost = laborCost;
        this.totalCost = totalCost;
        this.isPaid = isPaid;
    }

    public MaintenanceCostImpl(MaintenanceImpl maintenanceImpl, Integer materialCost, BigDecimal laborCost, BigDecimal totalCost, String isPaid) {
        this.maintenanceImpl = maintenanceImpl;
        this.materialCost = materialCost;
        this.laborCost = laborCost;
        this.totalCost = totalCost;
        this.isPaid = isPaid;
    }

    @Override
    public MaintenanceImpl getMaintenanceImpl() {
        return this.maintenanceImpl;
    }

    @Override
    public void setMaintenanceImpl(MaintenanceImpl maintenanceImpl) {
        this.maintenanceImpl= maintenanceImpl;
    }
    
    @Override
    public Integer getMaintenanceCostId() {
        return this.maintenanceCostId;
    }

    @Override
    public void setMaintenanceCostId(Integer maintenanceCostId) {
        this.maintenanceCostId= maintenanceCostId;
    }

    @Override
    public Integer getMaterialCost() {
        return this.materialCost;
    }

    @Override
    public void setMaterialCost(Integer materialCost) {
        this.materialCost= materialCost;
    }

    @Override
    public BigDecimal getLaborCost() {
        return this.laborCost;
    }

    @Override
    public void setLaborCost(BigDecimal laborCost) {
        this.laborCost= laborCost;
    }

    @Override
    public BigDecimal getTotalCost() {
        return this.totalCost;
    }

    @Override
    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost= totalCost;
    }

    @Override
    public String getIsPaid() {
        return this.isPaid;
    }

    @Override
    public void setIsPaid(String isPaid) {
        this.isPaid= isPaid;
    }
 
    public void toString2()
    {
        System.out.println("Maintenance Cost Id: " + maintenanceCostId + "Material Cost: " + materialCost + " LaborCost: " + laborCost + "Total Cost: " + totalCost + "Is Paid: " + isPaid);
    }
    
    private Integer maintenanceCostId;
    private MaintenanceImpl maintenanceImpl;
    private Integer materialCost;
    private BigDecimal laborCost;
    private BigDecimal totalCost;
    private String isPaid;
    
}
