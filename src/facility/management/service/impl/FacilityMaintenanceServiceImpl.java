/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service.impl;
import facility.management.dal.FacilityMaintenanceDAO;
import facility.management.model.maintenance.MaintenanceCostImpl;
import facility.management.model.maintenance.MaintenanceImpl;
import facility.management.model.maintenance.MaintenanceRequestImpl;
import facility.management.service.FacilityMaintenanceService;
import java.util.List;
public class FacilityMaintenanceServiceImpl implements FacilityMaintenanceService{

    public static void main(String[] args)
    {
        
    }
    @Override
    public Boolean makeFacilityMaintRequest(MaintenanceRequestImpl maintenanceRequestImpl) {
        return facilityMaintenanceDAO.addFacilityMaintenanceImplRequest(maintenanceRequestImpl);
    }

    @Override
    public Boolean scheduleMaintenance(MaintenanceImpl maintenanceImpl) {
        return facilityMaintenanceDAO.addScheduleMaintenance(maintenanceImpl);
    }

    @Override
    public Boolean calcMaintenanceCostForFacility(MaintenanceCostImpl maintenanceCostImpl) {
        return facilityMaintenanceDAO.calcMaintenanceCostForFacility(maintenanceCostImpl);
    }

    @Override
    public long calcProblemRateForFacility(int facilityId) {
        return facilityMaintenanceDAO.calcProblemRateForFacility(facilityId);
    }

    @Override
    public long calcDownTimeForFacility(int facilityId) {
        return facilityMaintenanceDAO.calcDownTimeForFacility(facilityId);
    }

    @Override
    public List<MaintenanceRequestImpl> listMaintRequests() {
        return facilityMaintenanceDAO.listMaintRequests();
    }

    @Override
    public List<MaintenanceImpl> listMaintenance() {
        return facilityMaintenanceDAO.listMaintenance();
    }

    @Override
    public List<String> listFacilityProblems(int facilityId) {
        return facilityMaintenanceDAO.getListOfFacilityProblems(facilityId);
    }
    
    FacilityMaintenanceDAO facilityMaintenanceDAO = new FacilityMaintenanceDAO();
    
    
}
