/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service.impl;

import facility.management.dal.FacilityDAO;
import facility.management.model.facility.BuildingImpl;
import facility.management.model.facility.FacilityImpl;
import facility.management.service.FacilityService;
import java.util.List;

/**
 *
 * @author dmartin3
 */
public class FacilityServiceImpl implements FacilityService {

    @Override
    public List<FacilityImpl> listFacilities() {
        return facilityDAO.listFacilities();
    }

    @Override
    public FacilityImpl getFacilityInformation(int facilityId) {
        return facilityDAO.getBuildingImplInfo(facilityId);
    }

    @Override
    public Long requestAvailableCapacity(int facilityId) {
        return facilityDAO.getAvailableCapacity(facilityId);
    }

    @Override
    public Boolean addNewFacility(FacilityImpl facilityImpl) {
        return facilityDAO.addNewFacility(facilityImpl);
    }

    public void addFacilityDetail(BuildingImpl buildingImpl) {
        facilityDAO.addBuildingDetails(buildingImpl);
    }

    @Override
    public Boolean removeFacility(int facilityId) {
        return facilityDAO.removeFacility(facilityId);
    }

    public FacilityDAO facilityDAO = new FacilityDAO();

}
