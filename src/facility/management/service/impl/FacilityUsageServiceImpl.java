/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service.impl;
import facility.management.dal.FacilityUsageDAO;
import facility.management.model.useage.InspectionImpl;
import facility.management.model.useage.LeaseInfoImpl;
import facility.management.service.FacilityUsageService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author dmartin3
 */
public class FacilityUsageServiceImpl implements FacilityUsageService {

    @Override
    public Boolean isInUseDuringInterval(int facilityId, Date beginDate, Date endDate) {
        return facilityUsageDAO.isInUseDuringInterval(facilityId, beginDate, endDate);
    }

    @Override
    public Boolean assignFacilityToUse(LeaseInfoImpl leaseInfoImpl) {
        return facilityUsageDAO.assignFacilityToUse(leaseInfoImpl);
    }

    @Override
    public Boolean vacateFacility(int facilityId, Date vacateDate) {
        return facilityUsageDAO.vacateFacility(facilityId, vacateDate);
    }

    @Override
    public List<InspectionImpl> listInspections() {
        return facilityUsageDAO.listInspections();
    }

    @Override
    public HashMap<Integer, Integer> listActualUsage() {
        return facilityUsageDAO.listActualUsage();
    }

    @Override
    public HashMap<Integer, BigDecimal> calcUsageRate() {
        return facilityUsageDAO.calcUsageRate();
    }
    
    private FacilityUsageDAO facilityUsageDAO = new FacilityUsageDAO();
            
            
}
