/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service;
import facility.management.model.maintenance.MaintenanceCostImpl;
import facility.management.model.maintenance.MaintenanceImpl;
import facility.management.model.maintenance.MaintenanceRequestImpl;

import java.util.List;
public interface FacilityMaintenanceService {

    public Boolean makeFacilityMaintRequest(MaintenanceRequestImpl maintenanceRequestImpl);
    public Boolean scheduleMaintenance(MaintenanceImpl maintenanceImpl);
    public Boolean calcMaintenanceCostForFacility(MaintenanceCostImpl maintenanceCostImpl);
    public long calcProblemRateForFacility(int facilityId);               	//per year
    public long calcDownTimeForFacility(int facilityId);                    	//in minutes
    public List<MaintenanceRequestImpl> listMaintRequests();
    public List<MaintenanceImpl> listMaintenance();
    public List<String> listFacilityProblems(int facilityId);
    
    
}