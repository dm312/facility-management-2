/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service;
import facility.management.model.useage.LeaseInfoImpl;
import facility.management.model.useage.InspectionImpl;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface FacilityUsageService {

    public Boolean isInUseDuringInterval(int facilityId, Date beginDate, Date endDate);
    public Boolean assignFacilityToUse(LeaseInfoImpl leaseInfoImpl);
    public Boolean vacateFacility(int facilityId, Date vacateDate);
    public List<InspectionImpl> listInspections();
    public HashMap<Integer, Integer> listActualUsage();           //usage in days for each facility
    public HashMap<Integer, BigDecimal> calcUsageRate();              //usage percentage (days used per year in percent)

}