/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facility.management.service;

import facility.management.model.facility.BuildingImpl;
import facility.management.model.facility.Facility;
import facility.management.model.facility.FacilityImpl;

import java.util.List;

public interface FacilityService {

    public List<FacilityImpl> listFacilities();
    public Facility getFacilityInformation(int facilityId);
    public Long requestAvailableCapacity(int facilityId);
    public Boolean addNewFacility(FacilityImpl facilityImpl);
    public void addFacilityDetail(BuildingImpl buildingImpl);
    public Boolean removeFacility(int facilityId);

}
